use std::collections::HashMap;

use dxvk_native::d3d11::{Buffer, ShaderResourceView, Texture2D};
use formats::flver::{self, Flver};

#[derive(Default)]
pub struct LevelResources {
    pub flvers: HashMap<String, (Flver<'static>, Buffer)>,
    pub textures: HashMap<String, (Texture2D, ShaderResourceView)>,
}

impl LevelResources {
    pub fn get_gpu_tex_pointer(
        &self,
        texture: &flver::texture::Texture,
    ) -> Option<&ShaderResourceView> {
        self.textures.get(&texture.file_name_only()).map(|e| &e.1)
    }
}
