use std::{
    collections::HashMap,
    ffi::c_void,
    fs,
    sync::{
        mpsc::{Receiver, Sender},
        Arc, Mutex,
    },
    time::SystemTime,
};

use dxvk_native::{
    d3d11::{
        self, BindFlag, Buffer, BufferDescription, ClearFlag, CullMode, FillMode, Filter, InputLayout, PixelShader, RasterizerDescription, RasterizerState, SamperDescription, SamplerState, Texture2DDescription, VertexShader
    },
    dxgi::{self, GetParent, SwapchainDescription1},
    QueryInterface,
};
use formats::{
    config,
    flver::texture,
    msb_2011::{model_part_type::ModelPartType, part::PartEntry},
    Vec3,
};
use game::game::Game;
use nalgebra::{Matrix4, Point3, Rotation3, Scale3, Translation3, Vector3};

use crate::{
    level_resources::LevelResources,
    loader::{spawn_loader, Request, Response},
    Device,
};

pub const MAIN_W: u32 = 1280;
pub const MAIN_H: u32 = 720;

#[derive(Debug, Default)]
#[repr(C)]
struct ConstantBuffer {
    world: Matrix4x4,
    view: Matrix4x4,
    projection: Matrix4x4,
    mist_dist: [f32; 4],
}

type Matrix4x4 = [f32; 16];

pub struct Renderer {
    dev: Device,
    ctx: d3d11::DeviceContext,
    swapchain: dxgi::SwapChain1,
    render_target_view: d3d11::RenderTargetView,
    depth_stencil_view: d3d11::DepthStencilView,

    rasterizer_states: RasterizerStates,
    sampler_state: SamplerState,

    constant_buffer: Buffer,

    /// Hard-coded shader. Will be in use until DX9 is implemented
    the_only_shader: Option<(VertexShader, PixelShader, InputLayout)>,
    vertex_shader_atime: u64,
    fragment_shader_atime: u64,
    shader_check_cooldown: u64,

    levels: HashMap<String, LevelResources>,

    pub(crate) ipc: (Sender<Request>, Receiver<Response>),
}

impl Renderer {
    pub(crate) fn init(hwnd: *mut c_void) -> Self {
        dxvk_native::set_full_hud();

        // Devices
        let dev = d3d11::create_device().unwrap();
        let ctx = dev.get_immediate_context();
        let dxgi_dev: dxgi::Device2 = dev.query_interface().unwrap();
        let dxgi_adp: dxgi::Adapter = dxgi_dev.get_adapter().unwrap();
        let dxgi_fac: dxgi::Factory2 = dxgi_adp.get_parent().unwrap();

        // Swapchain
        let swapchain_desc = SwapchainDescription1 {
            AlphaMode: dxgi::AlphaMode::STRAIGHT,
            ..dxgi::SwapchainDescription1::default()
        };
        let swapchain = dxgi_fac
            .create_swap_chain_for_hwnd(&dev, hwnd, swapchain_desc)
            .unwrap();

        // Depth stencil
        let depth_stencil_desc = Texture2DDescription {
            BindFlags: d3d11::BindFlag::DEPTH_STENCIL,
            ..Texture2DDescription::default(MAIN_W, MAIN_H, dxgi::Format::D24_UNORM_S8_UINT)
        };
        let depth_stencil_buffer = dev.create_texture_2d_empty(&depth_stencil_desc).unwrap();
        let depth_stencil_view = dev
            .create_depth_stencil_view(&depth_stencil_buffer)
            .unwrap();

        // Render target
        let backbuffer = swapchain.get_buffer(0).unwrap();
        let render_target_view = dev.create_render_target_view(&backbuffer).unwrap();
        ctx.om_set_render_targets(&[&render_target_view], Some(&depth_stencil_view));

        // Viewport
        let vp = d3d11::Viewport::standard(MAIN_W as f32, MAIN_H as f32);
        ctx.rs_set_viewports(&[vp]);

        let rs = RasterizerStates::new(&dev);

        let sampler_desc = SamperDescription {
            Filter: Filter::ANISOTROPIC,
            MaxAnisotropy: 16,
            ..SamperDescription::default()
        };
        let ss = dev
            .create_sampler_state(&sampler_desc)
            .unwrap();

        let bd = BufferDescription::default(size_of::<ConstantBuffer>(), BindFlag::CONSTANT_BUFFER);
        let constant_buffer = dev.create_buffer_empty(&bd).unwrap();

        let dev = Arc::new(Mutex::new(dev));
        let shaders = Renderer::load_shaders(dev.clone());

        let cfg = config::get_user_config("./user/cfg.json");
        let ipc = spawn_loader(cfg, dev.clone());

        Self {
            dev,
            ctx,
            swapchain,
            render_target_view,
            depth_stencil_view,
            rasterizer_states: rs,
            sampler_state: ss,
            constant_buffer,
            the_only_shader: Some(shaders),
            levels: HashMap::new(),
            ipc,
            vertex_shader_atime: accessed_millis("renderer/shaders/vs.bin"),
            fragment_shader_atime: accessed_millis("renderer/shaders/ps.bin"),
            shader_check_cooldown: 100,
        }
    }

    pub(crate) fn update(&mut self, game: &Game) {
        // Find levels we have to load
        for game_lvl in game.loaded_levels.keys() {
            if !self.levels.contains_key(game_lvl) {
                let level_name = game_lvl.clone();

                // First: add level to list -- otherwise we'll discard IPC responses later
                self.levels
                    .insert(game_lvl.clone(), LevelResources::default());

                // Second: actually make an IPC request
                self.ipc.0.send(Request::LoadLevel { level_name }).unwrap();
            }
        }

        // Unload all levels not loaded in game
        self.levels
            .retain(|k, _| game.loaded_levels.contains_key(k));

        self.shader_check_cooldown -= 1;
        if self.shader_check_cooldown == 0 {
            #[cfg(debug_assertions)]
            self.check_shaders();

            self.shader_check_cooldown = 100;
        }

        // Receive data from loader thread
        for _ in 0..1000 {
            let resp = self.ipc.1.try_recv();
            if resp.is_ok() {
                match resp.unwrap() {
                    Response::Texture {
                        level_name,
                        texture_name,
                        texture_data,
                    } => {
                        if !self.levels.contains_key(&level_name) {
                            continue;
                        }

                        self.levels
                            .get_mut(&level_name)
                            .unwrap()
                            .textures
                            .insert(texture_name, texture_data);
                    }
                    Response::Model {
                        level_name,
                        model_name,
                        model_data,
                    } => {
                        if !self.levels.contains_key(&level_name) {
                            continue;
                        }

                        self.levels
                            .get_mut(&level_name)
                            .unwrap()
                            .flvers
                            .insert(model_name, model_data);
                    }
                }
            } else {
                break;
            }
        }

        self.clear();

        for level in self.levels.keys() {
            if game.filter_map.as_ref().is_some_and(|f| f != level) {
                continue;
            }

            self.render_level(&game, level);
        }

        self.present(&game);
    }

    fn clear(&self) {
        self.ctx
            .clear_render_target_view(&self.render_target_view, [0.2, 0.4, 0.9, 1.0]);

        self.ctx.clear_depth_stencil_view(
            &self.depth_stencil_view,
            ClearFlag::Depth | ClearFlag::Stencil,
            1.0f32,
            0,
        );
    }

    fn render_level(&self, game: &Game, level_name: &String) {
        let msb = game.loaded_levels.get(level_name).unwrap();

        let models = msb.models();
        let parts = msb.parts();

        for part_idx in 0..parts.entry_count() {
            let part = parts.entry_by_idx(part_idx);
            let model = models.entry_by_idx(part.model_index as usize);

            if part.entry_type == ModelPartType::MapPiece {
                let name = model.name();

                if game
                    .filter_entity
                    .as_ref()
                    .is_some_and(|f| f != part.name())
                {
                    continue;
                }

                self.render_node(game, level_name, part, name);
            }
        }
    }

    fn render_node(&self, game: &Game, level_name: &String, part: &PartEntry, flver_name: &str) {
        let lvl = &self.levels.get(level_name).unwrap();

        let flver = lvl.flvers.get(flver_name);
        if flver.is_none() {
            return;
        }

        let (flver, data_buffer) = flver.unwrap();

        // Update and set constant buffer
        self.update_cbuffer(
            &part.position,
            &part.rotation,
            &part.scale,
            game.cam_position,
            game.cam_angle,
            game.mist_dist,
            &self.constant_buffer,
        );
        self.ctx
            .vs_set_constant_buffers(0, &[&self.constant_buffer]);

        let (vs, ps, layout) = self.the_only_shader.as_ref().unwrap();
        self.ctx.ia_set_input_layout(&layout);

        self.ctx.ps_set_samplers(0, &[&self.sampler_state]);

        for mesh in flver.mesh_iter() {
            let mat = mesh.header.material_index;

            for i in 0..mesh.surface_indices.len() {
                let surface = &flver.surfaces[mesh.surface_indices[i] as usize];

                self.ctx
                    .ia_set_primitive_topology(if surface.triangle_strip {
                        d3d11::PrimitiveTopology::TRIANGLESTRIP
                    } else {
                        d3d11::PrimitiveTopology::TRIANGLELIST
                    });

                // Set index buffer
                assert_eq!(surface.index_buffer_len / surface.indices_count, 2);
                self.ctx.ia_set_index_buffer(
                    &data_buffer,
                    dxgi::Format::R16_UINT,
                    surface.index_buffer_offset as usize,
                );

                // Set vertex buffer
                let vert_buf_index =
                    mesh.vertex_buffer_indices[i % mesh.vertex_buffer_indices.len()];
                let vert_buf = &flver.vertex_buffers[vert_buf_index as usize];

                let layout = flver.buffer_layout(vert_buf.layout_index);
                let pos_descr = layout.position();
                let pos_ofs = vert_buf.buffer_offset + pos_descr.struct_offset;

                let uv;
                let uv_pair = layout.uv_pair();
                if uv_pair.is_some() {
                    uv = uv_pair.unwrap();
                } else {
                    let uv_single = layout.uv();
                    if uv_single.is_some() {
                        uv = uv_single.unwrap();
                    } else {
                        todo!()
                    }
                }
                let uv_ofs = vert_buf.buffer_offset + uv.struct_offset;

                self.ctx.ia_set_vertex_buffers(
                    0,
                    &[&data_buffer, &data_buffer],
                    &[vert_buf.vertex_size; 2],
                    &[pos_ofs, uv_ofs],
                );

                let diffuse = flver.texture_by_type(mat, texture::DIFFUSE);
                if diffuse.is_some() {
                    let diffuse = diffuse.unwrap();
                    let l = lvl.get_gpu_tex_pointer(&diffuse);
                    if l.is_some() {
                        self.ctx.ps_set_shader_resources(0, &[l.unwrap()]);
                    }
                }

                self.ctx.rs_set_state(if surface.cull_backfaces {
                    &self.rasterizer_states.cull_backfaces
                } else {
                    &self.rasterizer_states.cull_none
                });

                // Render
                self.ctx.vs_set_shader(&vs);
                self.ctx.ps_set_shader(&ps);
                self.ctx.draw_indexed(surface.indices_count, 0, 0);
            }
        }
    }

    fn update_cbuffer(
        &self,
        model_pos: &Vec3,
        model_rot: &Vec3,
        model_scale: &Vec3,
        cam_position: Point3<f32>,
        cam_angle: [f32; 2],
        mist_dist: f32,
        constant_buffer: &Buffer,
    ) {
        let world = Matrix4::<f32>::identity()
            * Translation3::new(model_pos[0], model_pos[1], model_pos[2]).to_homogeneous()
            * Rotation3::from_euler_angles(
                deg2rad(model_rot[0]),
                deg2rad(model_rot[1]),
                deg2rad(model_rot[2]),
            )
            .to_homogeneous()
            * Scale3::new(model_scale[0], model_scale[1], model_scale[2]).to_homogeneous();

        let camera_target = Rotation3::from_euler_angles(cam_angle[0], cam_angle[1], 0.0)
            * Vector3::new(0.0_f32, 0.0, 1.0);

        let view = Matrix4::look_at_lh(
            &cam_position,
            &(cam_position + camera_target),
            &Vector3::new(0.0, 1.0, 0.0),
        );

        let fov = 3.14 / 2.75;
        let projection =
            nalgebra_glm::infinite_perspective_rh_zo(MAIN_W as f32 / MAIN_H as f32, fov, 0.1)
                * Scale3::new(-1.0, 1.0, 1.0).to_homogeneous();

        // Update variables
        let cb_data = ConstantBuffer {
            world: world.transpose().as_slice().try_into().unwrap(),
            view: view.transpose().as_slice().try_into().unwrap(),
            projection: projection.transpose().as_slice().try_into().unwrap(),
            mist_dist: [mist_dist; 4],
        };
        self.ctx.update_buffer(&constant_buffer, &cb_data);
    }

    fn present(&self, world: &Game) {
        let sync_interval = if world.use_vsync { 1 } else { 0 };
        self.swapchain.present(sync_interval, 0);
    }

    #[cfg(debug_assertions)]
    fn check_shaders(&mut self) {
        let vs_atime = accessed_millis("renderer/shaders/vs.bin");
        let ps_atime = accessed_millis("renderer/shaders/ps.bin");

        if vs_atime != self.vertex_shader_atime || ps_atime != self.fragment_shader_atime {
            let vs_data = fs::read("renderer/shaders/vs.bin").unwrap();
            let ps_data = fs::read("renderer/shaders/ps.bin").unwrap();

            let new_shader = Self::construct_shaders(self.dev.clone(), &vs_data, &ps_data);

            let _ = self.the_only_shader.replace(new_shader);

            self.vertex_shader_atime = vs_atime;
            self.fragment_shader_atime = ps_atime;
        }
    }

    fn load_shaders(dev: Device) -> (VertexShader, PixelShader, InputLayout) {
        let vs_data = include_bytes!("../shaders/vs.bin");
        let ps_data = include_bytes!("../shaders/ps.bin");

        Self::construct_shaders(dev, vs_data, ps_data)
    }

    fn construct_shaders(
        dev: Device,
        vs_data: &[u8],
        ps_data: &[u8],
    ) -> (VertexShader, PixelShader, InputLayout) {
        let vertex_shader = dev.lock().unwrap().create_vertex_shader(vs_data).unwrap();
        let pixel_shader = dev.lock().unwrap().create_pixel_shader(ps_data).unwrap();

        // Input layout
        let layout_desc = [
            d3d11::InputElementDescription::new(
                c"POSITION",
                0,
                dxgi::Format::R32G32B32_FLOAT,
                0,
                0,
                d3d11::InputClassification::PER_VERTEX_DATA,
            ),
            d3d11::InputElementDescription::new(
                c"TEXCOORD",
                0,
                dxgi::Format::R32_FLOAT,
                1,
                0,
                d3d11::InputClassification::PER_VERTEX_DATA,
            ),
        ];

        let layout = dev
            .lock()
            .unwrap()
            .create_input_layout(&layout_desc, vs_data)
            .unwrap();

        (vertex_shader, pixel_shader, layout)
    }
}

impl Drop for Renderer {
    fn drop(&mut self) {
        let _ = self.ipc.0.send(Request::Die);
        self.ipc.1.iter().for_each(|e| drop(e));
    }
}

struct RasterizerStates {
    pub cull_backfaces: RasterizerState,
    pub cull_none: RasterizerState,
}

impl RasterizerStates {
    pub fn new(dev: &d3d11::Device) -> Self {
        Self {
            cull_backfaces: dev
                .create_rasterizer_state(&RasterizerDescription {
                    FillMode: FillMode::SOLID,
                    CullMode: CullMode::BACK,
                    ..RasterizerDescription::zeroed()
                })
                .unwrap(),

            cull_none: dev
                .create_rasterizer_state(&RasterizerDescription {
                    FillMode: FillMode::SOLID,
                    CullMode: CullMode::NONE,
                    ..RasterizerDescription::zeroed()
                })
                .unwrap(),
        }
    }
}

fn deg2rad(deg: f32) -> f32 {
    deg * 3.1415 / 180.0
}

fn accessed_millis(watched_lib_file: &str) -> u64 {
    fs::metadata(watched_lib_file)
        .unwrap()
        .accessed()
        .unwrap()
        .duration_since(SystemTime::UNIX_EPOCH)
        .unwrap()
        .as_millis() as u64
}
