use std::{
    ffi::c_void,
    sync::{Arc, Mutex},
};

use dxvk_native::d3d11;
use game::game::Game;
use renderer::Renderer;

mod level_resources;
mod loader;
mod renderer;

pub type Device = Arc<Mutex<d3d11::Device>>;

/// Opaque handle to the `Renderer` structure.
///
/// This is in fact a type-erased heap pointer.
///
/// It is required for hot-reloading.
pub struct RendererOpaque {
    ptr: usize,
}

impl RendererOpaque {
    fn init(hwnd: *mut c_void) -> Self {
        let renderer = Renderer::init(hwnd);
        let b = Box::new(renderer);
        let raw = Box::into_raw(b);
        let ptr = raw as usize;
        RendererOpaque { ptr }
    }
    

    fn access_mut(&mut self) -> &mut Renderer {
        unsafe { (self.ptr as *mut Renderer).as_mut().unwrap() }
    }
}

impl Drop for RendererOpaque {
    fn drop(&mut self) {
        let b = unsafe { Box::from_raw(self.ptr as *mut Renderer) };
        drop(b);
    }
}

#[no_mangle]
pub fn init(hwnd: *mut c_void) -> RendererOpaque {
    RendererOpaque::init(hwnd)
}

/// Do regular per-frame stuff (render, ipc poll, etc)
#[no_mangle]
pub fn update(renderer: &mut RendererOpaque, game: &Game) {
    renderer.access_mut().update(game);
}
