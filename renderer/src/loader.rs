use std::{
    collections::HashMap,
    sync::mpsc::{Receiver, Sender},
    thread,
};

use dxvk_native::{
    d3d11::{
        self, BindFlag, Buffer, BufferDescription, ShaderResourceView, ShaderResourceViewDesc,
        SrvDimension, Texture2D, Texture2DDescription, Texture2DSubResourceView,
    },
    dxgi,
};

use formats::{
    archive::{NestedArchive, RootArchive},
    flver::{self, Flver},
    msb_2011::{model_part_type::ModelPartType, Msb2011},
    tpf::Tpf,
};
use image_dds::ddsfile::Dds;

use crate::Device;

pub enum Request {
    Die,
    LoadLevel { level_name: String },
}

pub enum Response {
    Texture {
        level_name: String,
        texture_name: String,
        texture_data: (Texture2D, ShaderResourceView),
    },
    Model {
        level_name: String,
        model_name: String,
        model_data: (Flver<'static>, Buffer),
    },
}

pub(crate) fn spawn_loader(
    cfg: formats::config::UserConfig,
    dev: Device,
) -> (Sender<Request>, Receiver<Response>) {
    let (rq_tx, rq_rx) = std::sync::mpsc::channel();
    let (rz_tx, rz_rx) = std::sync::mpsc::channel();

    thread::spawn(move || {
        let archives = [
            RootArchive::new(&cfg, 0),
            RootArchive::new(&cfg, 1),
            RootArchive::new(&cfg, 2),
            RootArchive::new(&cfg, 3),
        ];

        loop {
            let r = rq_rx.recv().unwrap_or(Request::Die);
            match r {
                Request::Die => {
                    return;
                }
                Request::LoadLevel { level_name } => {
                    load_level(dev.clone(), &archives, &rz_tx, &level_name);
                }
            }
        }
    });

    (rq_tx, rz_rx)
}

pub fn load_level(
    dev: Device,
    archives: &[RootArchive<'static>; 4],
    tx: &Sender<Response>,
    map_name: &str,
) {
    let map_index = &map_name[1..3];
    let msb_name = format!("/map/MapStudio/{map_name}.msb");

    let msb = Msb2011::new(archives[0].data_by_name(&msb_name).unwrap());
    let tex_archives = archives[0].map_texture_archives(map_index);

    let mut flvers: HashMap<String, ()> = HashMap::new();
    let mut textures: HashMap<String, ()> = HashMap::new();

    let models = msb.models();
    for model_idx in 0..models.entry_count() {
        // if model_idx != 111 {
        //     continue;
        // }

        let model = models.entry_by_idx(model_idx);

        if model.entry_type == ModelPartType::MapPiece {
            let model_name = model.name().to_string();
            let flver_path = format!("/map/{map_name}/{model_name}A{map_index}.flver.dcx");
            let flver = load_flver(dev.clone(), archives, &flver_path);

            if flver.is_none() {
                continue;
            }

            let flver = flver.unwrap();

            for th in flver.0.textures {
                if !th.is_valid {
                    continue;
                }

                let parsed_th = flver::texture::Texture::new(&flver.0, th);
                let name = parsed_th.file_name_only();

                if !textures.contains_key(&name) {
                    let tex = load_texture(dev.clone(), &name, &tex_archives);

                    if tex.is_none() {
                        continue;
                    }

                    let tex = tex.unwrap();
                    tx.send(Response::Texture {
                        level_name: map_name.to_string(),
                        texture_name: name.clone(),
                        texture_data: tex,
                    })
                    .unwrap();

                    textures.insert(name, ());
                }
            }

            tx.send(Response::Model {
                level_name: map_name.to_string(),
                model_name: model_name.clone(),
                model_data: flver,
            })
            .unwrap();

            flvers.insert(model_name, ());
        }
    }
}

fn load_flver(
    dev: Device,
    archives: &[RootArchive<'static>; 4],
    flver_path: &str,
) -> Option<(Flver<'static>, Buffer)> {
    let data = archives[0].data_by_name(flver_path).unwrap();
    let data_len = data.len();

    let flver = Flver::new(data);
    let flver_data = flver.data_section();

    let buffer_desc = BufferDescription::default(
        data_len - flver.header.data_offset as usize,
        BindFlag::VERTEX_BUFFER | BindFlag::INDEX_BUFFER,
    );
    let buffer = dev
        .lock()
        .unwrap()
        .create_buffer(&buffer_desc, flver_data.as_ptr(), 0, 0);

    if buffer.is_err() {
        println!("Somehow failed to create buffer? Bro WTF?!");
        return None;
    }

    Some((flver, buffer.unwrap()))
}

fn load_texture(
    dev: Device,
    file_name: &String,
    texture_archives: &Vec<NestedArchive>,
) -> Option<(Texture2D, ShaderResourceView)> {
    // TODO: there's may be a way to look up texture faster than complete
    for archive in texture_archives {
        let data = archive.lookup_data(file_name);

        if data.is_some() {
            return Some(load_texture_from_data(dev, data.unwrap()));
        }
    }

    println!("Unable to look up tex {file_name} in texture achives!");
    None
}

fn load_texture_from_data(dev: Device, data: Vec<u8>) -> (Texture2D, ShaderResourceView) {
    let tpf = Tpf::new(data);

    let dds = Dds::read(tpf.dds(0)).unwrap();
    let image = image_dds::image_from_dds(&dds, 0).unwrap();

    // Here's the problem:
    // - `image_dds`'s buffer contains vertical slices of pixels;
    // - DirectX wants horizontal slices;
    //
    // Solution: transform buffer to match DX's expectations
    let image = image_dds::image::imageops::rotate90(&image);
    let image = image_dds::image::imageops::flip_horizontal(&image);

    let format = dxgi::Format::R8G8B8A8_UNORM;

    let base_tex_desc = Texture2DDescription {
        MipLevels: 1,
        Usage: d3d11::Usage::DEFAULT,
        BindFlags: BindFlag::SHADER_RESOURCE | BindFlag::RENDER_TARGET,
        MiscFlags: /* GENERATE_MIPMAPS = */ 1,
        ..Texture2DDescription::default(image.width(), image.height(), format)
    };

    let base_tex = dev
        .lock()
        .unwrap()
        .create_texture_2d(
            &base_tex_desc,
            image.as_ptr(),
            4 * image.width(),
            4 * image.width() * image.height(),
        )
        .unwrap();

    let srv_desc = ShaderResourceViewDesc {
        Format: format,
        ViewDimension: SrvDimension::TEXTURE2D,
        Data: d3d11::ShaderResourceViewDescData {
            Texture2D: Texture2DSubResourceView {
                MostDetailedMip: 0,
                MipLevels: 1,
            },
        },
    };

    let desc = Some(&srv_desc);

    let base_view = dev
        .lock()
        .unwrap()
        .create_shader_resource_view(desc, &base_tex)
        .unwrap();

    // ctx.update_texture(
    //     &base_tex,
    //     unsafe { image.as_ptr().as_ref().unwrap() },
    //     4 * image.width(),
    //     4 * image.width() * image.height(),
    // );
    // ctx.generate_mips(&base_view);

    (base_tex, base_view)
}
