// fxc.exe /T vs_5_0 /E VS /Fo vs.bin shaders.hlsl
// fxc.exe /T ps_5_0 /E PS /Fo ps.bin shaders.hlsl

cbuffer ConstantBuffer : register( b0 ) {
	matrix world;
	matrix view;
	matrix proj;
    float mist_distance;
}

Texture2D base_texture;
SamplerState base_sampler;

struct VS_OUTPUT {
    float4 pos : SV_POSITION;
    float2 uv : TEXCOORD;
    float mist_dist : UNICORN;
};

// uv is two i16's
VS_OUTPUT VS (float4 pos : POSITION, int uv : TEXCOORD ) {
    VS_OUTPUT output;
    
    output.pos = mul(mul(mul(pos, world), view), proj);

    // UV coords are packed into one i32 as pair of two i16s
    int left_i16 = (uv >> 16);
    int right_i16 = (uv << 16) >> 16;

    float2 unpacked_uv = {
        float(left_i16) / 1024.0,
        float(right_i16) / 1024.0,
    };
    output.uv = unpacked_uv;
    output.mist_dist = mist_distance;
    
    return output;
}


float Noise(float2 uv){
    return frac(sin((uv.x+uv.y)*199.f)*123.f);
}

// Source: unknown
float Dither(float2 uv, float v, float colorCount)  {
  float c = v * colorCount;

  //this checks whether the colors last bit(s) is/are above a random number and therefore rounds randomly
  c += Noise(uv) > frac(c) ? 1.0 : 0.0;
  c -= frac(c);
  c /= colorCount;
  
  return c;          
}


float4 PS (VS_OUTPUT input) : SV_Target {
    float4 tex = base_texture.Sample(base_sampler, input.uv);
    clip(tex.a - 0.5);

    // It turned out, dithering is useless

    float4 mist = { 0.75, 0.75, 1.0, 1.0 };
    float v1 = input.pos.w / input.mist_dist;
    float4 v = { v1, v1, v1, v1 };
    
    return lerp(tex, mist, v);
}
