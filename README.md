# DSDS

Blazingly fast but almost useless DS1 map viewer.

[Documentation](https://somobu.gitlab.io/dsds/book/index.html) | [Reference](https://somobu.gitlab.io/dsds/doc/dantelion/index.html) | [Dev videos](https://www.youtube.com/playlist?list=PLPT7KABCtFvxiAJ53m-8yKnMyfb9IEac9)

> GitLab started doing weird things to my CI, so Documentation and Reference links are outdated. You can build it yourself using `cargo doc` and `mdbook` utilities.


## How to build and run

First of all, you'll need a Steam version of DS1 (PTDE) installed.

Clone repo, `./run` once, adjust generated config in `user/cfg.json`.


## Backlog

Objectives:
- render map using original vpo/fpo;
    - dear Somobu, please take a look at "Shaders stuff" section below;
    - `dxvk-native` still lacks dx9 bindings;
- handle all msb `Parts` (objbnd, anibnd, hkx, nbmbnd);
- render map w/ map objects;

Unresolved:
- how to jump from material definition (.mtd) to corresponding shader pair (.vpo + .fpo)?
- Undead Burg and Duke's Archives placed incorrectly -- is there some obscude MSB data somewhere else?
- the use of msb1/part draw & disp groups is not known;

Unimplmeneted:
- map props (MSB 'object' type - objbnd);
- msb1: events and regions;
- map collision;

Future:
- replace all `assert` w/ `debug_assert`;
- handle map triggers;
- but what is ESD (format)?;
- handle scripts (use bytecode? decompile? both?);
- room for improvement: it is possible to load data using pre-allocated memory chunks (no `malloc` at all);

Long term:
- Ash Lake -- `map/m13_01_00_00/`
- Quelaag's Domain (Fair Lady) -- `map/m14_00_00_00/m51x0B0A14.flver` (ex.: `m5100B0A14.flver`)
- Majula
- Devotee Scarlett
- Very distant future: it'll be fun to patch DS2 map to make it feel more like DS1 (fair LODs, interconnectivity),


### Shaders stuff

Dear Somobu, I hope you'll be able to remember what you were doing c. October 
2024. To make your life easier I add some notes here.

What you wanted to do: take compiled DS1 shaders (they are DX9 shaders, shader
model 5.0), and somehow use it in your rendering process. Also the inputs of
the shaders is unknown and undocumented so you also have to figure it out 
somehow.

There are two options.

First option,: you can use [HLSLcc][hlslcc]. It is adopted by Unity and looks 
exactly like what you wanted.

If HLSLcc is unusable, you can fall back to DXVK converter. It is able to 
convert DX shaders to SPIRV format, from which you can go anywhere. But I think
this way is harder -- you'll have to mess with all the DXVK project, which 
looks so scary to me.


[hlslcc]: https://github.com/Unity-Technologies/HLSLcc


## Credits

- [Souls Formats](https://github.com/JKAnderson/SoulsFormats/tree/master) by [Joseph Anderson](https://github.com/JKAnderson);
- [dstools](https://github.com/katalash/dstools/) by [katalash](https://github.com/katalash);
- [ds_extract_and_pack](https://github.com/MasonM/ds_extract_and_pack) by [Mason Malone](https://github.com/MasonM);
- [blender-flver](https://github.com/kotn3l/blender-flver) by [Eliza](https://github.com/elizagamedev) and [kotn3l](https://github.com/kotn3l)


## License

[GNU GPL v3](https://gitlab.com/somobu/dsds/-/blob/master/LICENSE?ref_type=heads)
