//! Handles Binder stuff (bhd/bdt, bhf/bdf), provides access to archive data by hash or path
//!
//! Additionally performs in-place DCX decompression

use memmap2::Mmap;

use crate::{
    bhd5::{Bhd5, Record},
    bhf3::Bhf3,
    config::UserConfig,
    dcx, fhash,
};

/// Top-level archive, encapsulates .bhd5 + .bdt files access
pub struct RootArchive<'a> {
    index: Bhd5<'a>,
    data: Mmap,
}

impl<'a> RootArchive<'a> {
    pub fn new(cfg: &UserConfig, index: u8) -> RootArchive<'a> {
        let header_file = cfg.file(&format!("dvdbnd{}.bhd5", index)).unwrap();
        let header_data = unsafe { Mmap::map(&header_file) }.unwrap();

        let data_file = cfg.file(&format!("dvdbnd{}.bdt", index)).unwrap();
        let data_data = unsafe { Mmap::map(&data_file) }.unwrap();

        RootArchive {
            index: Bhd5::new(header_data),
            data: data_data,
        }
    }

    /// Prints comma-separated file offset, padded file size and file name hash
    pub fn print_human_index(&self) {
        for record in self.index.record_iter() {
            println!(
                "{:x?},{},{}",
                record.file_offset,
                record.get_padded_file_size(),
                fhash::rev(record.file_name_hash)
            );
        }
    }

    pub fn record_by_name(&self, path: &str) -> Option<&Record> {
        self.index.get_record_by_hash(fhash::hash(path))
    }

    pub fn record_by_hash(&self, hash: u32) -> Option<&Record> {
        self.index.get_record_by_hash(hash)
    }

    /// Handles DCX decompression as well
    pub fn data_by_name(&self, path: &str) -> Option<Vec<u8>> {
        self.data_by_hash(fhash::hash(path))
    }

    /// Handles DCX decompression as well
    pub fn data_by_hash(&self, hash: u32) -> Option<Vec<u8>> {
        let record = self.record_by_hash(hash);
        if record.is_none() {
            return None;
        }

        let record = record.unwrap();

        let file_offset = record.file_offset as usize;

        let magic = &self.data[file_offset..(file_offset + 4)];
        let data = &self.data[file_offset..(file_offset + record.get_padded_file_size() as usize)];

        let rz;
        if dcx::is_dcx(&magic) {
            rz = dcx::decompress_file(data);
        } else {
            rz = data.to_vec();
        }

        return Some(rz);
    }

    pub fn map_texture_archives(&self, map_index: &str) -> Vec<NestedArchive> {
        let mut rz: Vec<NestedArchive> = vec![];

        for n in 0..4 {
            let index = self
                .data_by_name(&format!("/map/m{map_index}/m{map_index}_{n:0>4}.tpfbhd"))
                .unwrap();

            let record = self
                .record_by_name(&format!("/map/m{map_index}/m{map_index}_{n:0>4}.tpfbdt"))
                .unwrap();

            let archive = NestedArchive::new(index, &self.data, record.file_offset);

            rz.push(archive);
        }

        rz
    }
}

/// Binder access, bhf3 + bdf3 files pair (ex.: .tpfbhd + .ptfbdt)
pub struct NestedArchive<'a> {
    pub index: Bhf3<'a>,
    data: &'a [u8],
}

impl<'a> NestedArchive<'a> {
    pub fn new(index_data: Vec<u8>, file: &'a Mmap, data_offset: u64) -> NestedArchive<'a> {
        let index = Bhf3::new(index_data);

        NestedArchive {
            index,
            data: &file[(data_offset as usize)..],
        }
    }

    /// Search archive for a record by name (file name w/o path and extension)
    pub fn lookup_data(&self, name_only: &str) -> Option<Vec<u8>> {
        for record in self.index.records {
            let name = self.index.record_name_by_offset(record.filename_offset);

            if name.contains(name_only) {
                let start = record.offset as usize;
                let len = record.compressed_size as usize;

                let magic = &self.data[start..(start + 4)];

                let rz;
                if dcx::is_dcx(&magic) {
                    rz = dcx::decompress_file(&self.data[start..(start + len)]);
                } else {
                    rz = self.data[start..(start + len)].to_vec();
                }

                return Some(rz);
            }
        }

        None
    }
}
