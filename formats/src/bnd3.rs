//! A general-purpose file container used in DS1. Extension: .*bnd
//!
//! Definition based on [SoulsFormats](https://github.com/JKAnderson/SoulsFormats)

use std::{mem::size_of, ptr::slice_from_raw_parts};

use crate::{bnd_flags, magic, Validate};

const MAGIC_HEADER: [u8; 4] = *b"BND3";

pub fn is_bnd(data: &[u8]) -> bool {
    if data.len() < 4 {
        return false;
    }

    data[0..4] == MAGIC_HEADER
}

#[repr(C)]
struct Bnd3Header {
    magic_header: [u8; 4],

    id: [u8; 8],

    flags: u8,
    padding_1: [u8; 3],

    record_count: u32,
    header_size: u32,

    padding_2: [u8; 8],
}

impl Bnd3Header {
    fn is(&self, flag: u8) -> bool {
        &self.flags & flag != 0
    }
}

impl Validate for Bnd3Header {
    fn validate(&self) {
        assert_eq!(self.magic_header, MAGIC_HEADER);
        assert_eq!(self.padding_1, [0u8; 3]);
        assert_eq!(self.padding_2, [0u8; 8]);
    }
}

#[allow(non_camel_case_types)]
#[repr(C)]
struct Record_Names1_LongOffset {
    flags: u8,
    padding_1: [u8; 3],

    data_size_compressed: u32,
    data_offset: u32,
    id: i32,

    filename_offset: i32,

    data_size_uncompressed: u32,
}

impl Validate for Record_Names1_LongOffset {
    fn validate(&self) {
        assert_eq!(self.padding_1, [0u8; 3]);
    }
}

#[repr(C)]
struct Record {
    flags: u8,
    padding: [u8; 3],

    data_size: u32,
    data_offset: u32,

    id: i32,

    filename_offset: i32,
}

impl Validate for Record {
    fn validate(&self) {
        assert_eq!(self.padding, [0u8; 3]);
    }
}

pub struct Bnd3<'a> {
    data: Vec<u8>,
    header: &'a Bnd3Header,
}

impl<'a> Bnd3<'a> {
    pub fn new(data: Vec<u8>) -> Bnd3<'a> {
        let header = unsafe { (data.as_ptr() as *const Bnd3Header).as_ref().unwrap() };
        header.validate();

        Bnd3 { data, header }
    }

    pub fn flags(&self) -> u8 {
        self.header.flags
    }

    /// List records ("Names 1" and "Long Offsets" must be set)
    fn records_n1_lo(&self) -> &'a [Record_Names1_LongOffset] {
        assert!(&self.header.is(bnd_flags::Names1));
        assert!(&self.header.is(bnd_flags::LongOffsets));

        unsafe {
            let ptr =
                self.data.as_ptr().add(size_of::<Bnd3Header>()) as *const Record_Names1_LongOffset;

            slice_from_raw_parts(ptr, self.header.record_count as usize)
                .as_ref()
                .unwrap()
        }
    }

    /// Looks up data by `name` substring. Implemented for a specific subset of bnd (for now).
    pub fn lookup(&self, name: &str) -> Option<&[u8]> {
        for record in self.record_iter() {
            let record_name = record.name();

            if record_name.contains(name) {
                return Some(record.data());
            }
        }

        None
    }

    /// List records (assuming "Names 1" and "Long Offsets" flags are set)
    pub fn record_iter<'b>(&'b self) -> IndexIter<'b> {
        assert!(&self.header.is(bnd_flags::Names1));
        assert!(&self.header.is(bnd_flags::LongOffsets));

        IndexIter {
            index: self,
            current_idx: -1,
        }
    }
}

pub struct Bnd3Record<'a> {
    bnd: &'a Bnd3<'a>,
    pub data_offset: u32,
    pub data_len: u32,
    name_offset: i32,
}

impl<'a> Bnd3Record<'a> {
    pub fn name(&self) -> &'a str {
        unsafe { magic::ascii_by_vec_offset(&self.bnd.data, self.name_offset) }
    }

    /// Direct reference to data (probably compressed)
    pub fn data(&self) -> &'a [u8] {
        let start = /*size_of::<Bnd3Header>() + */ self.data_offset as usize;
        let end = start + self.data_len as usize;
        return &self.bnd.data.as_slice()[start..end];
    }
}

pub struct IndexIter<'a> {
    index: &'a Bnd3<'a>,
    current_idx: i32,
}

impl<'a> Iterator for IndexIter<'a> {
    type Item = Bnd3Record<'a>;

    fn next(&mut self) -> Option<Self::Item> {
        self.current_idx += 1;

        if self.current_idx < self.index.header.record_count as i32 {
            let offset = size_of::<Bnd3Header>()
                + self.current_idx as usize * size_of::<Record_Names1_LongOffset>();

            let record = unsafe {
                (self.index.data.as_ptr().add(offset) as *const Record_Names1_LongOffset)
                    .as_ref()
                    .unwrap()
            };

            return Some(Bnd3Record {
                bnd: self.index,
                data_offset: record.data_offset,
                data_len: record.data_size_compressed,
                name_offset: record.filename_offset,
            });
        }

        None
    }
}
