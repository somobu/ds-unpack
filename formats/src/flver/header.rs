//! FLVER2 header. Used since DS1. Extension: .flv, .flver

use crate::Vec3;

const MAGIC_HEADER: [u8; 6] = *b"FLVER\0";

#[repr(C)]
pub struct FlverHeader {
    magic: [u8; 6],

    /// 'L' (little-endian) or 'B' (big-endian)
    endianess: u8,
    padding_1: u8,

    /// - DS1: 2000C, 2000D
    /// - DS2: 20009, 20010
    /// - SFS: 20010
    /// - BB:  20013, 20014
    /// - DS3: 20013, 20014
    /// - SDT: 2001A
    /// - ER:  ?????
    pub version: u32,

    pub data_offset: u32,
    pub data_size: u32,
    pub dummy_count: i32,
    pub material_count: i32,
    pub bone_count: i32,
    pub mesh_count: i32,
    pub vertex_buffer_count: i32,

    pub bbox_min: Vec3,
    pub bbox_max: Vec3,

    /// Face count not including motion blur meshes or degenerate faces
    pub face_count: u32,
    pub total_face_count: i32,

    /// 0x00 or 0x10
    pub default_vertex_index_size: u8,

    pub unicode: bool,
    pub unknown_4: bool,
    padding_2: u8,

    /// Padding?
    pub unknown_5: i16,

    /// -1 or 0
    pub unknown_6: i16,

    pub surface_count: i32,
    pub buffer_layout_count: u32,
    pub texture_count: i32,

    pub unknown_7: i32,
    padding_3: [i32; 2],

    /// In range 0..=4
    pub unknown_8: i32,

    padding_4: [i32; 5],
}

impl FlverHeader {
    pub fn validate(&self) {
        assert_eq!(self.endianess, b'L', "Little-Endian data is expected");
        assert_eq!(self.padding_1, 0);
        assert_eq!(self.padding_2, 0);
        assert_eq!(self.padding_3, [0, 0]);
        assert_eq!(self.padding_4, [0; 5]);

        // NOTE: uv_divisor is 2048 since 0x2000F
        assert!(
            self.version == 0x2000C || self.version == 0x2000D,
            "Only DS1 (wrong version {:x})!",
            self.version
        );

        assert!(
            self.default_vertex_index_size == 0x00
                || self.default_vertex_index_size == 0x08
                || self.default_vertex_index_size == 0x10
                || self.default_vertex_index_size == 0x20
        );

        assert_eq!(self.unicode, true);
        assert!(self.unknown_6 == -1 || self.unknown_6 == 0);
        assert!(self.unknown_8 >= 0 && self.unknown_8 <= 4);
    }
}
