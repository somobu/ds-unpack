//! Determines how vertices in a mesh are connected to form triangles.

use std::slice;

use super::Flver;

#[repr(C)]
pub struct SurfaceHeader {
    pub flags: u32,

    /// Whether vertices are defined as a triangle strip or individual triangles
    pub triangle_strip: bool,

    /// Whether triangles can be seen through from behind
    pub cull_backfaces: bool,

    /// Padding? Reserved?
    unknown: [u8; 2],

    pub indices_count: u32,

    /// Offset from flver data start
    pub index_buffer_offset: u32,
    pub index_buffer_len: u32,

    padding_2: i32,

    /// Non-zero since BB (>=0x20013)
    pub index_size: i32,
    padding_3: i32,
}

impl SurfaceHeader {
    pub fn validate(&self) {
        assert_eq!(self.unknown, [0; 2], "This assertion may be wrong");
        assert_eq!(self.padding_2, 0);
        assert_eq!(self.padding_3, 0);

        assert!(
            self.index_size == 0
                || self.index_size == 8
                || self.index_size == 16
                || self.index_size == 32
        );
    }
}

/// Flags on a faceset, mostly just used to determine lod level.
#[derive(Debug)]
#[repr(u32)]
pub enum Flags {
    /// Just your average everyday face set.
    None = 0,

    /// Low detail mesh.
    LodLevel1 = 0x0100_0000,

    /// Really low detail mesh.
    LodLevel2 = 0x0200_0000,

    /// Not confirmed, but suspected to indicate when indices are edge-compressed.
    EdgeCompressed = 0x4000_0000,

    /// Many meshes have a copy of each faceset with and without this flag. If you remove them, motion blur stops working.
    MotionBlur = 0x8000_0000,
}

pub struct Surface<'a> {
    flver: &'a Flver<'a>,
    pub header: &'a SurfaceHeader,
    pub indices: &'a [u16],
}

impl<'a> Surface<'a> {
    pub fn new(flver: &'a Flver, header: &'a SurfaceHeader) -> Surface<'a> {
        header.validate();

        let indices = unsafe {
            let offset = flver.header.data_offset + header.index_buffer_offset;
            let pointer = flver.data.as_ptr().add(offset as usize);

            slice::from_raw_parts(pointer as *const u16, header.indices_count as usize)
        };

        Surface {
            flver,
            header,
            indices,
        }
    }
}
