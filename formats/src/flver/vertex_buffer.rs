//! A block of vertex data

use std::slice;

use super::Flver;

/// Raw header
#[repr(C)]
pub struct VertexBufferHeader {
    pub buffer_index: u32,

    /// Index to a layout in the FLVER's layout collection
    pub layout_index: u32,

    /// Size of single vertex, in bytes
    pub vertex_size: u32,
    pub vertex_count: u32,

    padding: [u32; 2],

    /// = layout_size * vertex_count
    pub buffer_length: u32,
    pub buffer_offset: u32,
}

impl VertexBufferHeader {
    pub fn validate(&self) {
        assert_eq!(self.padding, [0; 2]);
    }
}

pub struct VertexBuffer<'a> {
    flver: &'a Flver<'a>,
    pub header: &'a VertexBufferHeader,
}

impl<'a> VertexBuffer<'a> {
    pub fn new(flver: &'a Flver, header: &'a VertexBufferHeader) -> VertexBuffer<'a> {
        header.validate();
        VertexBuffer { flver, header }
    }

    pub fn vertices(&self) -> VertexIterator {
        VertexIterator {
            vb: self,
            next_item: 0,
        }
    }
}

pub struct VertexIterator<'a> {
    vb: &'a VertexBuffer<'a>,
    next_item: u32,
}

impl<'a> Iterator for VertexIterator<'a> {
    type Item = &'a [u8];

    fn next(&mut self) -> Option<Self::Item> {
        let header = self.vb.header;

        if self.next_item >= header.vertex_count {
            return None;
        }

        let vertex = unsafe {
            let offset = self.vb.flver.header.data_offset
                + header.buffer_offset
                + self.next_item * header.vertex_size;
            let pointer = self.vb.flver.data.as_ptr().add(offset as usize);

            slice::from_raw_parts(pointer as *const u8, header.vertex_size as usize)
        };

        self.next_item += 1;

        Some(vertex)
    }
}
