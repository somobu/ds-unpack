use core::slice;
use std::mem::size_of;

use crate::Vec3;

use super::Flver;

/// Determines which properties of a vertex are read and written, and in what order and format

#[repr(C)]
pub struct BufferLayoutHeader {
    pub members_count: i32,

    padding: [i32; 2],

    pub members_offset: i32,
}

impl BufferLayoutHeader {
    pub fn validate(&self) {
        assert_eq!(self.padding, [0; 2]);
    }
}

#[repr(C)]
pub struct LayoutMember {
    unknown: i32,

    /// Offset within vertex data
    pub struct_offset: u32,

    /// Format used to store this member
    pub layout_type: LayoutType,

    /// Vertex property being stored
    pub semantic: LayoutSemantic,

    /// For semantics that may appear more than once such as UVs, which one this member is
    pub index: i32,
}

impl LayoutMember {
    pub fn validate(&self) {
        assert!([0, 1, 2].contains(&self.unknown));
    }

    pub fn extract_position(&self, vertex: &[u8]) -> &Vec3 {
        assert_eq!(self.layout_type, LayoutType::Float3);
        assert_eq!(self.semantic, LayoutSemantic::Position);
        assert!(self.struct_offset as usize + size_of::<Vec3>() < vertex.len());

        unsafe {
            (vertex.as_ptr().add(self.struct_offset as usize) as *const Vec3)
                .as_ref()
                .unwrap()
        }
    }

    pub fn extract_4b(&self, vertex: &[u8]) -> &[u8; 4] {
        unsafe {
            (vertex.as_ptr().add(self.struct_offset as usize) as *const [u8; 4])
                .as_ref()
                .unwrap()
        }
    }

    pub fn extract_2s(&self, vertex: &[u8]) -> &[i16; 2] {
        unsafe {
            (vertex.as_ptr().add(self.struct_offset as usize) as *const [i16; 2])
                .as_ref()
                .unwrap()
        }
    }

    pub fn extract_4s(&self, vertex: &[u8]) -> &[i16; 4] {
        unsafe {
            (vertex.as_ptr().add(self.struct_offset as usize) as *const [i16; 4])
                .as_ref()
                .unwrap()
        }
    }
}

/// Format of a vertex property.
#[derive(Debug, PartialEq, Eq)]
#[repr(u32)]
pub enum LayoutType {
    /// Two single-precision floats.
    Float2 = 0x01,

    /// Three single-precision floats.
    Float3 = 0x02,

    /// Four single-precision floats.
    Float4 = 0x03,

    /// Unknown.
    Byte4A = 0x10,

    /// Four bytes.
    Byte4B = 0x11,

    /// Two shorts?
    Short2toFloat2 = 0x12,

    /// Four bytes w/ color data (?)
    Byte4C = 0x13,

    /// Two shorts.
    UV = 0x15,

    /// Two shorts and two shorts.
    UVPair = 0x16,

    /// Four shorts, maybe unsigned?
    ShortBoneIndices = 0x18,

    /// Four shorts.
    Short4toFloat4A = 0x1A,

    /// Unknown.
    Short4toFloat4B = 0x2E,

    /// Unknown.
    Byte4E = 0x2F,

    /// Unknown but appears to be another form of edge compression; not actually supported.
    EdgeCompressed = 0xF0,
}

impl LayoutType {
    pub fn size(&self) -> usize {
        match self {
            LayoutType::EdgeCompressed => 1,
            LayoutType::Byte4A | LayoutType::Byte4B => 4,
            LayoutType::Byte4C | LayoutType::Byte4E => 4,
            LayoutType::Short2toFloat2 | LayoutType::UV => 4,
            LayoutType::Float2 => 8,
            LayoutType::Short4toFloat4A | LayoutType::Short4toFloat4B => 8,
            LayoutType::UVPair | LayoutType::ShortBoneIndices => 8,
            LayoutType::Float3 => 12,
            LayoutType::Float4 => 16,
        }
    }
}

/// Property of a vertex.
#[derive(Debug, PartialEq)]
#[repr(u32)]
pub enum LayoutSemantic {
    /// Location of the vertex.
    Position = 0,

    /// Weight of the vertex's attachment to bones.
    BoneWeights = 1,

    /// Bones the vertex is weighted to, indexing the parent mesh's bone indices.
    BoneIndices = 2,

    /// Orientation of the vertex.
    Normal = 3,

    /// Texture coordinates of the vertex.
    UV = 5,

    /// Vector pointing perpendicular to the normal.
    Tangent = 6,

    /// Vector pointing perpendicular to the normal and tangent.
    Bitangent = 7,

    /// Data used for blending, alpha, etc.
    VertexColor = 10,
}

pub struct BufferLayout<'a> {
    flver: &'a Flver<'a>,
    pub header: &'a BufferLayoutHeader,
    pub members: &'a [LayoutMember],
}

impl<'a> BufferLayout<'a> {
    pub fn new(flver: &'a Flver<'a>, header: &'a BufferLayoutHeader) -> BufferLayout<'a> {
        header.validate();

        unsafe {
            let pointer = flver.data.as_ptr().add(header.members_offset as usize);
            let members = slice::from_raw_parts(
                pointer as *const LayoutMember,
                header.members_count as usize,
            );

            BufferLayout {
                flver,
                header,
                members,
            }
        }
    }

    pub fn position(&self) -> &'a LayoutMember {
        for member in self.members {
            if member.semantic == LayoutSemantic::Position {
                return member;
            }
        }

        panic!("Unable to get position semantic!");
    }

    pub fn normal(&self) -> Option<&'a LayoutMember> {
        for member in self.members {
            if member.semantic == LayoutSemantic::Normal {
                return Some(member);
            }
        }

        return None;
    }

    pub fn tangent(&self) -> Option<&'a LayoutMember> {
        for member in self.members {
            if member.semantic == LayoutSemantic::Tangent {
                return Some(member);
            }
        }

        return None;
    }

    pub fn vertex_color(&self) -> Option<&'a LayoutMember> {
        for member in self.members {
            if member.semantic == LayoutSemantic::VertexColor {
                return Some(member);
            }
        }

        return None;
    }

    /// Search for [LayoutType::UV] type
    pub fn uv(&self) -> Option<&'a LayoutMember> {
        for member in self.members {
            if member.layout_type == LayoutType::UV {
                return Some(member);
            }
        }

        return None;
    }

    /// Search for [LayoutType::UVPair] type
    pub fn uv_pair(&self) -> Option<&'a LayoutMember> {
        for member in self.members {
            if member.layout_type == LayoutType::UVPair {
                return Some(member);
            }
        }

        return None;
    }
}
