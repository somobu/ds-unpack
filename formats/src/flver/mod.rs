//! Model format used since DS1. Extension: .flv, .flver
//!
//! File spec and comments taken from [SoulsFormats](https://github.com/JKAnderson/SoulsFormats/)
//! and [DS tools](https://github.com/katalash/dstools/)

use std::{mem::size_of, slice};

use crate::{
    flver::{buffer_layout::BufferLayout, material::Material, vertex_buffer::VertexBuffer},
    magic::utf16le_by_vec_offset,
};

use self::{
    bone::Bone,
    buffer_layout::BufferLayoutHeader,
    dummy::Dummy,
    header::FlverHeader,
    mesh::{Mesh, MeshHeader},
    surface::{Surface, SurfaceHeader},
    texture::{Texture, TextureHeader},
    vertex_buffer::VertexBufferHeader,
};

pub mod bone;
pub mod buffer_layout;
pub mod dummy;
pub mod header;
pub mod material;
pub mod mesh;
pub mod surface;
pub mod texture;
pub mod vertex_buffer;

/// Struct to handle Flver data
pub struct Flver<'a> {
    data: Vec<u8>,

    pub header: &'a FlverHeader,

    pub dummies: &'a [Dummy],
    pub materials: &'a [Material],
    pub bones: &'a [Bone],
    pub meshes: &'a [MeshHeader],
    pub surfaces: &'a [SurfaceHeader],
    pub vertex_buffers: &'a [VertexBufferHeader],
    pub buffer_layouts: &'a [BufferLayoutHeader],
    pub textures: &'a [TextureHeader],
}

impl<'a> Flver<'a> {
    pub fn new(data: Vec<u8>) -> Flver<'a> {
        unsafe {
            let mut pointer = data.as_ptr();

            let header = (pointer as *const FlverHeader).as_ref().unwrap();
            header.validate();
            pointer = pointer.add(size_of::<FlverHeader>());

            let dummy_count = header.dummy_count as usize;
            let dummies = slice::from_raw_parts(pointer as *const Dummy, dummy_count);
            pointer = pointer.add(dummy_count * size_of::<Dummy>());

            let mat_count = header.material_count as usize;
            let mats = slice::from_raw_parts(pointer as *const Material, mat_count);
            pointer = pointer.add(mat_count * size_of::<Material>());

            let bone_count = header.bone_count as usize;
            let bones = slice::from_raw_parts(pointer as *const Bone, bone_count);
            pointer = pointer.add(bone_count * size_of::<Bone>());

            let mesh_count = header.mesh_count as usize;
            let mesh = slice::from_raw_parts(pointer as *const MeshHeader, mesh_count);
            pointer = pointer.add(mesh_count * size_of::<MeshHeader>());

            let fs_count = header.surface_count as usize;
            let fss = slice::from_raw_parts(pointer as *const SurfaceHeader, fs_count);
            pointer = pointer.add(fs_count * size_of::<SurfaceHeader>());

            let vb_count = header.vertex_buffer_count as usize;
            let vbs = slice::from_raw_parts(pointer as *const VertexBufferHeader, vb_count);
            pointer = pointer.add(vb_count * size_of::<VertexBufferHeader>());

            let bl_count = header.buffer_layout_count as usize;
            let bls = slice::from_raw_parts(pointer as *const BufferLayoutHeader, bl_count);
            pointer = pointer.add(bl_count * size_of::<BufferLayoutHeader>());

            let tex_count = header.texture_count as usize;
            let textures = slice::from_raw_parts(pointer as *const TextureHeader, tex_count);
            // pointer = pointer.add(tex_count * size_of::<Texture>());

            let flver = Flver {
                data,
                header,
                dummies,
                materials: mats,
                bones,
                meshes: mesh,
                surfaces: fss,
                vertex_buffers: vbs,
                buffer_layouts: bls,
                textures,
            };

            flver
        }
    }

    pub fn mesh(&self, index: i32) -> Mesh {
        Mesh::new(&self, &self.meshes[index as usize])
    }

    pub fn mesh_iter(&'a self) -> MeshIter<'a> {
        MeshIter {
            index: self,
            current_idx: -1,
        }
    }

    pub fn surface(&self, index: i32) -> Surface {
        Surface::new(&self, &self.surfaces[index as usize])
    }

    pub fn vertex_buffer(&self, index: i32) -> VertexBuffer {
        VertexBuffer::new(&self, &self.vertex_buffers[index as usize])
    }

    pub fn buffer_layout(&self, index: u32) -> BufferLayout {
        BufferLayout::new(&self, &self.buffer_layouts[index as usize])
    }

    pub fn texture(&self, index: i32) -> Texture {
        Texture::new(&self, &self.textures[index as usize])
    }

    pub fn texture_by_type(&self, mat: i32, tex_type: &str) -> Option<Texture> {
        let mat = &self.materials[mat as usize];
        let s = mat.texture_index as usize;
        let e = s + mat.texture_count as usize;

        for tex in &self.textures[s..e] {
            let t = Texture::new(&self, tex);

            if t.type_name() == tex_type {
                return Some(t);
            }
        }

        None
    }

    pub fn data_section(&'a self) -> &'a [u8] {
        let from = self.header.data_offset as usize;
        let to = (self.header.data_offset + self.header.data_size) as usize;
        &self.data[from..to]
    }

    pub fn to_data_offset(&self, from_flver_start: u32) -> u32 {
        from_flver_start - self.header.data_offset
    }

    pub fn print_contents(&self) {
        let header = self.header;
        println!("Header ver 0x{:x}", header.version);

        println!("Material count: {}", self.materials.len());
        for i in 0..self.materials.len() {
            let material = &self.materials[i];
            material.validate();

            println!("  Name {}", unsafe {
                utf16le_by_vec_offset(&self.data, material.name_offset)
            });
            println!("  MTD {}", unsafe {
                utf16le_by_vec_offset(&self.data, material.mtd_offset)
            });
            println!("  GX {}", material.gx_offset);

            println!("  {} textures", material.texture_count);

            for i in material.texture_index..(material.texture_index + material.texture_count) {
                let tex = &self.texture(i);
                println!("    {} {}", tex.type_name(), tex.normal_path())
            }
        }

        println!("Bone count: {}", self.bones.len());
        for i in 0..self.bones.len() {
            let bone = &self.bones[i];
            bone.validate();

            println!("  bone");
        }

        println!("Mesh count: {}", self.header.mesh_count);
        for index in 0..self.header.mesh_count {
            let mesh = &self.mesh(index);
            mesh.header.validate();

            println!(
                "  {} bones, {} faces, {} VBs",
                mesh.header.bone_count,
                mesh.surface_indices.len(),
                mesh.vertex_buffer_indices.len()
            );
        }

        println!("Face set count {}", self.header.surface_count);
        for index in 0..self.header.surface_count {
            let face = &self.surface(index);
            face.header.validate();

            println!("  #{}: {} indices", index, face.indices.len(),);
        }

        println!("Vertex buffer count {}", header.vertex_buffer_count);
        for index in 0..header.vertex_buffer_count {
            let vert_buf = &self.vertex_buffer(index);
            vert_buf.header.validate();

            println!(
                "  {} vertices, layout size {}",
                vert_buf.header.vertex_count, vert_buf.header.vertex_size,
            );
        }

        println!("Buffer layout count {}", header.buffer_layout_count);
        for index in 0..header.buffer_layout_count {
            let bl = self.buffer_layout(index);
            bl.header.validate();

            println!(
                "  layout from 0x{:x}, len {}",
                bl.header.members_offset, bl.header.members_count
            );
        }

        println!();
        println!();
    }
}

pub struct SurfaceIter<'a> {
    index: &'a Flver<'a>,
    current_idx: i32,
}

impl<'a> Iterator for SurfaceIter<'a> {
    type Item = Surface<'a>;

    fn next(&mut self) -> Option<Self::Item> {
        self.current_idx += 1;

        if self.current_idx < self.index.header.surface_count as i32 {
            return Some(self.index.surface(self.current_idx));
        }

        None
    }
}

pub struct MeshIter<'a> {
    index: &'a Flver<'a>,
    current_idx: i32,
}

impl<'a> Iterator for MeshIter<'a> {
    type Item = Mesh<'a>;

    fn next(&mut self) -> Option<Self::Item> {
        self.current_idx += 1;

        if self.current_idx < self.index.header.mesh_count as i32 {
            return Some(self.index.mesh(self.current_idx));
        }

        None
    }
}
