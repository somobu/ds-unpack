//! A texture used by the shader specified in an MTD.

use crate::{magic, Validate, Vec2};

use super::Flver;

pub const DIFFUSE: &'static str = "g_Diffuse";
pub const DIFFUSE_2: &'static str = "g_Diffuse_2";

pub const SPECULAR: &'static str = "g_Specular";
pub const SPECULAR_2: &'static str = "g_Specular_2";

pub const BUMPMAP: &'static str = "g_Bumpmap";
pub const BUMPMAP_2: &'static str = "g_Bumpmap_2";

#[repr(C)]
pub struct TextureHeader {
    path_name_offset: i32,
    type_name_offset: i32,

    pub scale: Vec2,

    /// - Value within 0..2
    /// - 0 if path_name is empty, non-zero otherwise
    pub unknown_1: u8,

    /// True if path_name is not empty, false otherwise
    pub is_valid: bool,
    padding_1: [u8; 2],

    /// Effectively empty in DS1 game data
    padding_2: [i32; 3],
}

impl Validate for TextureHeader {
    fn validate(&self) {
        assert_eq!(self.padding_1, [0; 2]);
        assert_eq!(self.padding_2, [0; 3]);
        assert!(self.unknown_1 == 0 || self.unknown_1 == 2 || self.unknown_1 == 2);
    }
}

#[derive(Clone)]
pub struct Texture<'a> {
    flver: &'a Flver<'a>,
    pub header: &'a TextureHeader,
}

impl<'a> Texture<'a> {
    pub fn new(flver: &'a Flver, header: &'a TextureHeader) -> Texture<'a> {
        Texture { flver, header }
    }

    /// Network path to the texture file to use.
    pub fn path(&self) -> String {
        assert_eq!(self.flver.header.unicode, true);
        unsafe { magic::utf16le_by_vec_offset(&self.flver.data, self.header.path_name_offset) }
    }

    /// Normalizes debug pathname
    pub fn normal_path(&self) -> String {
        let mut path = self.path().replace("\\", "/");

        if path.starts_with("N:/FRPG") {
            // Skip "N:\FRPG\data\Model" part
            path = path[18..].to_string();
        }

        path
    }

    /// Texture file name, extension included
    pub fn file_name_only(&self) -> String {
        let file_path = self.normal_path();
        let last_shash = file_path.rfind("/").unwrap() + 1;
        let dot = file_path.find(".").unwrap();

        file_path[last_shash..dot].to_string()
    }

    /// The type of texture this is, corresponding to the entries in the MTD.
    pub fn type_name(&self) -> String {
        assert_eq!(self.flver.header.unicode, true);
        unsafe { magic::utf16le_by_vec_offset(&self.flver.data, self.header.type_name_offset) }
    }
}
