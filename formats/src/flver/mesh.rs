use std::slice;

use super::Flver;

#[repr(C)]
pub struct MeshHeader {
    /// Wether mesh is in bind pose. Most likely has further implications.
    dynamic: bool,
    padding_1: [u8; 3],

    /// Index of the material used by all triangles in this mesh.
    pub material_index: i32,

    /// Until 0x20010 (inclusive, SFS) padding is 2 i32s long
    /// From 0x20011 and above padding is 1 i32 long
    padding_2: [i32; 2],

    /// Apparently does nothing. Usually points to a dummy bone named after the model, possibly just for labelling.
    default_bone_index: i32,

    pub bone_count: i32,

    /// Optional bounding box struct (null if 0)
    /// Since 0x20013 (inclusive, DS3)
    pub bounding_box_offset: i32,

    pub bone_offset: i32,

    /// Think of (`surface_indices_offset`, `surface_indices_count`) as of
    /// pointer to array of surface inices located somewhere else
    surface_indices_count: i32,
    surface_indices_offset: i32,

    vertex_buffer_count: i32,
    vertext_buffer_offset: i32,
}

impl MeshHeader {
    pub fn validate(&self) {
        assert_eq!(self.bounding_box_offset, 0, "Should be empty in DS1");
        assert_eq!(self.padding_1, [0; 3]);
        assert_eq!(self.padding_2, [0; 2]);
    }
}

pub struct Mesh<'a> {
    flver: &'a Flver<'a>,
    pub header: &'a MeshHeader,
    pub surface_indices: &'a [i32],
    pub vertex_buffer_indices: &'a [i32],
}

impl<'a> Mesh<'a> {
    pub fn new(flver: &'a Flver, header: &'a MeshHeader) -> Mesh<'a> {
        header.validate();

        unsafe {
            let pointer = flver
                .data
                .as_ptr()
                .add(header.surface_indices_offset as usize);
            let surface_indices =
                slice::from_raw_parts(pointer as *const i32, header.surface_indices_count as usize);

            let pointer = flver
                .data
                .as_ptr()
                .add(header.vertext_buffer_offset as usize);
            let vertex_buffers =
                slice::from_raw_parts(pointer as *const i32, header.vertex_buffer_count as usize);

            Mesh {
                flver,
                header,
                surface_indices,
                vertex_buffer_indices: vertex_buffers,
            }
        }
    }
}
