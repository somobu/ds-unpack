//! Group - map part

use crate::{magic, Validate, Vec3};

use super::model_part_type::ModelPartType;

#[repr(C)]
pub struct PartEntry {
    name_offset: i32,

    pub entry_type: ModelPartType,
    pub id: i32,
    pub model_index: i32,

    /// SIBs are not present in release data (some kind of editor placeholder?)
    sib_offset: i32,

    /// Location of the part
    pub position: Vec3,

    /// Rotation of the part, in degrees
    pub rotation: Vec3,

    /// Scale of the part, only meaningful for map pieces and objects
    pub scale: Vec3,

    /// Controls when the part is visible
    pub draw_groups: [u32; 4],

    /// Controls when the part is visible
    pub disp_groups: [u32; 4],

    entity_data_offset: i32,
    type_data_offset: i32,

    padding: i32,
}

impl PartEntry {
    pub fn sib(&self) -> Option<&str> {
        unsafe { magic::ascii_by_relative_offset(self, self.sib_offset) }
    }

    pub fn name(&self) -> &str {
        unsafe { magic::ascii_by_relative_offset(self, self.name_offset) }.unwrap()
    }

    pub fn entity_data<'a>(&'a self) -> &'a EntityData {
        let ptr = self as *const PartEntry as *const u8;
        let ptr = unsafe { ptr.add(self.entity_data_offset as usize) };

        let data = unsafe { (ptr as *const EntityData).as_ref() }.unwrap();
        data.validate();

        return data;
    }

    pub fn type_data<'a, T: Validate>(&'a self) -> &'a T {
        let ptr = self as *const PartEntry as *const u8;
        let ptr = unsafe { ptr.add(self.type_data_offset as usize) };

        let data = unsafe { (ptr as *const T).as_ref() }.unwrap();
        data.validate();

        return data;
    }
}

impl Validate for PartEntry {
    fn validate(&self) {
        assert_eq!(self.padding, 0);
    }
}


#[derive(Debug)]
#[repr(C)]
pub struct EntityData {
    pub entity_id: i32,

    pub light_id: u8,
    pub fog_id: u8,
    pub scatter_id: u8,
    pub lens_flare_id: u8,
    pub shadow_id: u8,
    pub dof_id: u8,
    pub tone_map_id: u8,
    pub tone_correct_id: u8,
    pub lantern_id: u8,
    pub load_param_id: i8,

    padding_1: u8,

    pub is_shadow_src: bool,
    pub is_shadow_dst: bool,
    pub is_shadow_only: bool,

    pub draw_by_reflect_cam: bool,
    pub draw_only_reflect_cam: bool,

    pub use_depth_bias_float: bool,
    pub disable_point_light_effect: bool,

    padding_2: [u8; 2],
}

impl Validate for EntityData {
    fn validate(&self) {
        assert_eq!(self.padding_1, 0);
        assert_eq!(self.padding_2, [0; 2]);
    }
}

/// Type data for [ModelPartType::Object] and [ModelPartType::DummyObject]
#[derive(Debug)]
#[repr(C)]
pub struct ObjectTypeData {
    padding_1: u32,

    /// Collision that controls loading of the object.
    pub collision_index: i32,

    /// Unknown
    pub break_term: i8,

    /// Unknown
    pub net_sync_type: i8,

    padding_2: u16,

    /// Unknown
    pub init_anim_i32: i16,

    /// Unknown
    pub unknown_1: i16,

    /// Unknown
    pub unknown_2: i32,

    padding_3: u32,
}

impl Validate for ObjectTypeData {
    fn validate(&self) {
        assert_eq!(self.padding_1, 0);
        assert_eq!(self.padding_2, 0);
        assert_eq!(self.padding_3, 0);
    }
}

/// Type data for [ModelPartType::Enemy] and [ModelPartType::DummyEnemy]
#[derive(Debug)]
#[repr(C)]
pub struct EnemyTypeData {
    padding_1: [u32; 2],

    /// ID in NPCThinkParam determining AI properties
    pub think_param_id: i32,

    /// ID in NPCParam determining character properties
    pub npc_param_id: i32,

    /// ID of a talk ESD used by the character
    pub talk_id: i32,

    /// Unknown
    pub point_move_type: u8,

    padding_2: u8,

    /// Unknown
    pub platoon_id: u16,

    /// ID in CharaInitParam determining equipment and stats for humans
    pub chara_init_id: i32,

    /// Collision that controls loading of the enemy
    pub collision_index: i32,

    padding_3: [i32; 2],

    /// Regions for the enemy to patrol
    pub move_point_indices: [i16; 8],

    /// Unknown
    pub init_anim_id: i32,

    /// Unknown
    pub damage_anim_id: i32,
}

impl Validate for EnemyTypeData {
    fn validate(&self) {
        assert_eq!(self.padding_1, [0; 2]);
        assert_eq!(self.padding_2, 0);
        assert_eq!(self.padding_3, [0; 2]);
    }
}

/// Type data for [ModelPartType::Collision]
#[derive(Debug)]
#[repr(C)]
pub struct CollisionTypeData {
    /// Unknown
    pub hit_filter_id: u8,

    /// Causes sounds to be modulated when standing on the collision
    pub sound_space_type: u8,

    /// Unknown
    pub env_light_map_spot_index: i16,

    /// Unknown
    pub reflect_plane_height: f32,

    /// Unknown
    pub nvm_groups: [u32; 4],

    /// Unknown
    pub vagrant_entity_ids: [i32; 3],

    /// Controls displays of the map name on screen or the loading menu
    pub map_name_id: i16,

    /// Unknown
    pub disable_start: bool,

    /// Padding?
    pub unknown: u8,

    /// If set, disables a bonfire when any enemy is on the collision
    pub disable_bonfire_entity_id: i32,

    minus_ones: [i32; 3],

    /// An ID used for multiplayer eligibility
    pub play_region_id: i32,

    /// ID in LockCamParam determining camera properties
    pub lock_cam_param_id_1: i16,

    /// ID in LockCamParam determining camera properties
    pub lock_cam_param_id_2: i16,

    padding: [i32; 4],
}

impl Validate for CollisionTypeData {
    fn validate(&self) {
        assert_eq!(self.minus_ones, [-1; 3]);
        assert_eq!(self.padding, [0; 4]);
    }
}

/// Type data for [ModelPartType::Navmesh]
#[derive(Debug)]
#[repr(C)]
pub struct NavmeshTypeData {
    /// Unknown
    pub nvm_groups: [u32; 4],

    padding: [u32; 4],
}

impl Validate for NavmeshTypeData {
    fn validate(&self) {
        assert_eq!(self.padding, [0; 4]);
    }
}

/// Type data for [ModelPartType::ConnectCollision]
#[derive(Debug)]
#[repr(C)]
pub struct ConnectCollisionTypeData {
    /// The collision which will load another map
    pub collision_index: i32,

    /// Four bytes specifying the map ID to load
    pub map_id: [u8; 4],

    padding: [u32; 2],
}

impl Validate for ConnectCollisionTypeData {
    fn validate(&self) {
        assert_eq!(self.padding, [0; 2]);
    }
}
