//! A map layout file used in DS1. Extension: .msb
//!
//! File spec and comments taken from [SoulsFormats](https://github.com/JKAnderson/SoulsFormats/)
//! and [DS tools](https://github.com/katalash/dstools/)

use std::{marker::PhantomData, mem::size_of};

use crate::{msb_2011::group::Group, Validate};

use self::{
    event::EventEntry, group::MsbGroup, model::ModelEntry, part::PartEntry, region::RegionEntry,
};

pub mod event;
pub mod group;
pub mod model;
pub mod model_part_type;
pub mod part;
pub mod region;

/// Struct to handle MSB data
pub struct Msb2011 {
    pub(self) data: Vec<u8>,
    model_offset: i32,
    event_offset: i32,
    point_offset: i32,
    parts_offset: i32,
}

impl Msb2011 {
    pub fn new(data: Vec<u8>) -> Msb2011 {
        let mut msb = Msb2011 {
            data,
            model_offset: 0,
            event_offset: 0,
            point_offset: 0,
            parts_offset: 0,
        };

        // TODO: we can check wether msb is big endian or not
        // Src: https://github.com/JKAnderson/SoulsFormats/blob/master/SoulsFormats/Formats/MSB/MSB1/MSB1.cs#L66

        let l = {
            let model = msb.group_by_offset::<ModelEntry>(0);
            assert_eq!(model.name(), "MODEL_PARAM_ST");

            let event = model.next::<EventEntry>();
            assert_eq!(event.name(), "EVENT_PARAM_ST");

            let point = event.next::<RegionEntry>();
            assert_eq!(point.name(), "POINT_PARAM_ST");

            let parts = point.next::<PartEntry>();
            assert_eq!(parts.name(), "PARTS_PARAM_ST");

            (event.offset, point.offset, parts.offset)
        };

        msb.event_offset = l.0;
        msb.point_offset = l.1;
        msb.parts_offset = l.2;

        msb
    }

    /// Model files that are available for parts to use.
    pub fn models(&self) -> MsbGroup<ModelEntry> {
        self.group_by_offset(self.model_offset)
    }

    /// Dynamic or interactive systems such as item pickups, levers, enemy spawners, etc.
    pub fn events(&self) -> MsbGroup<EventEntry> {
        self.group_by_offset(self.event_offset)
    }

    /// Points or areas of space that trigger some sort of behavior.
    pub fn regions(&self) -> MsbGroup<RegionEntry> {
        self.group_by_offset(self.point_offset)
    }

    /// Instances of actual things in the map.
    pub fn parts(&self) -> MsbGroup<PartEntry> {
        self.group_by_offset(self.parts_offset)
    }

    fn group_by_offset<T: Validate>(&self, offset: i32) -> MsbGroup<T> {
        assert!(offset >= 0);

        let offset = offset as usize;
        assert!(offset < (self.data.len() - size_of::<Group>()));

        let group_ptr = unsafe { (self.data.as_ptr() as *const u8).add(offset) };
        let group = unsafe { (group_ptr as *const Group).as_ref().unwrap() };

        group.validate();

        return MsbGroup::<T> {
            offset: offset as i32,
            msb: self,
            group,
            d: PhantomData,
        };
    }

    fn entry_by_offset<T: Validate>(&self, offset: i32) -> &T {
        assert!(offset >= 0);

        let offset = offset as usize;
        assert!(offset < (self.data.len() - size_of::<T>()));

        let entry_ptr = unsafe { (self.data.as_ptr() as *const u8).add(offset) };
        let entry = unsafe { (entry_ptr as *const T).as_ref().unwrap() };

        entry.validate();

        return entry;
    }
}
