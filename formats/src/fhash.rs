//! Filename hashing algorithm, used in archive indices

use std::{collections::HashMap, fs::File, io::Read};

use once_cell::sync::Lazy;

/// Hash from path
pub fn hash(path: &str) -> u32 {
    if path.len() == 0 {
        return 0;
    }

    let bytes = path.as_bytes();

    let mut accum = 0;

    if bytes[0] != b'/' {
        accum += b'/' as u32;
    }

    for b in bytes {
        let byte;

        // Brain-dead to-lower-case conversion
        if *b >= b'A' && *b <= b'Z' {
            byte = *b + 0x20;
        } else {
            byte = *b;
        }

        accum = accum.wrapping_mul(37);
        accum = accum.wrapping_add(byte as u32);
    }

    return accum;
}

static mut REV_HASH_MAP: Lazy<HashMap<u32, String>> = Lazy::new(|| HashMap::new());

pub fn fill_revhash_map(path: &str) {
    let file = File::open(path);
    if file.is_err() {
        eprintln!("Fill revhash: unable to find {path}");
        return;
    }

    let mut file = file.unwrap();
    let mut d = String::new();
    file.read_to_string(&mut d).unwrap();

    for name in d.lines() {
        unsafe { REV_HASH_MAP.insert(hash(&name), name.to_string()) };
    }
}

/// Path from hash (hard-coded, implemented only for top-level indices)
pub fn rev(hash: u32) -> String {
    unsafe {
        assert!(
            REV_HASH_MAP.len() > 0,
            "You probably want to `fill_revhash_map`"
        );
        REV_HASH_MAP[&hash].clone()
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn simple_path() {
        assert_eq!(hash("/map/m14_00_00_00/m5100B0A14.flver.dcx"), 84490027);
    }
}
