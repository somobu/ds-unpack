#![allow(non_snake_case, non_upper_case_globals, dead_code)]

/// File is big-endian regardless of the big-endian byte
pub const BigEndian: u8 = 0b0000_00001;

/// Files have ID numbers
pub const IDs: u8 = 0b0000_00010;

/// Files have name strings; Names2 may or may not be set.
///
/// Perhaps the distinction is related to whether it's a full path or just the filename?
pub const Names1: u8 = 0b0000_0100;

/// Files have name strings; Names1 may or may not be set
pub const Names2: u8 = 0b0000_1000;

/// File data offsets are 64-bit
pub const LongOffsets: u8 = 0b0001_0000;

/// Files may be compressed
pub const Compressed: u8 = 0b0010_0000;

/// Unknown
pub const Flag6: u8 = 0b0100_0000;

/// Unknown
pub const Flag7: u8 = 0b1000_0000;
