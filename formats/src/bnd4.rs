//! A general-purpose file container after DS1. Extension: .*bnd
//!
//! Definition based on [SoulsFormats](https://github.com/JKAnderson/SoulsFormats)

use std::{mem::size_of, ptr::slice_from_raw_parts};

use crate::{magic, Validate};

const MAGIC_HEADER: [u8; 4] = *b"BND4";

#[derive(Debug)]
#[repr(C)]
pub struct Bnd4Header {
    magic_header: [u8; 4],

    unknown_1: bool,
    unknown_2: bool,
    padding_1: [u8; 2],

    padding_2: u8,
    big_endinan: bool,
    bit_big_endinan: bool,
    padding_3: u8,

    record_count: u32,
    unknown_3: u64,

    version: [u8; 8],

    header_size: u64,
    header_end: u64,

    unicode: bool,
    format: u8,
    extended: u8,
    padding_4: u8,

    padding_5: u32,
    hash_table_offset: u64,
}

impl Validate for Bnd4Header {
    fn validate(&self) {
        assert_eq!(self.magic_header, MAGIC_HEADER);
        assert_eq!(self.padding_1, [0u8; 2]);
        assert_eq!(self.padding_2, 0);
        assert_eq!(self.padding_3, 0);
        assert_eq!(self.padding_4, 0);
        assert_eq!(self.padding_5, 0);

        assert_eq!(self.big_endinan, false);
        assert_eq!(self.bit_big_endinan, true);
        assert_eq!(self.unknown_3, 0x40);

        assert_eq!(self.format, 0b_0101_0100);
        assert_eq!(self.unicode, true);
    }
}

#[allow(non_camel_case_types)]
#[repr(C)]
struct Bnd4FileHeader {
    flags: u8,
    padding_1: [u8; 3],

    all_f: i32,

    compressed_size: i32,

    zeroes: i32,
    uncompressed_size: i32,

    zeroes_again: i32,
    data_offset: i32,

    /// Some kind of ID or index
    id: i32,

    filename_offset: u32,
}

impl Validate for Bnd4FileHeader {
    fn validate(&self) {
        assert_eq!(self.padding_1, [0u8; 3]);
        assert_eq!(self.all_f, -1);
        assert_eq!(self.zeroes, 0);
        assert_eq!(self.zeroes_again, 0);
    }
}

pub struct Bnd4<'a> {
    data: Vec<u8>,
    pub header: &'a Bnd4Header,
}

impl<'a> Bnd4<'a> {
    pub fn new(data: Vec<u8>) -> Bnd4<'a> {
        let header = unsafe { (data.as_ptr() as *const Bnd4Header).as_ref().unwrap() };
        header.validate();

        Bnd4 { data, header }
    }

    /// List records ("Names 1" and "Long Offsets" must be set)
    fn records_n1_lo(&self) -> &'a [Bnd4FileHeader] {
        unsafe {
            let ptr = self.data.as_ptr().add(size_of::<Bnd4Header>()) as *const Bnd4FileHeader;

            slice_from_raw_parts(ptr, self.header.record_count as usize)
                .as_ref()
                .unwrap()
        }
    }

    /// Looks up data by `name` substring. Implemented for a specific subset of bnd (for now).
    pub fn lookup(&self, name: &str) -> Option<&[u8]> {
        for record in self.record_iter() {
            let record_name = record.name();

            if record_name.contains(name) {
                return Some(record.data());
            }
        }

        None
    }

    /// List records (assuming "Names 1" and "Long Offsets" flags are set)
    pub fn record_iter<'b>(&'b self) -> IndexIter<'b> {
        IndexIter {
            index: self,
            current_idx: -1,
        }
    }
}

pub struct Bnd4Record<'a> {
    bnd: &'a Bnd4<'a>,
    pub data_offset: u64,
    pub data_len: u64,
    name_offset: u32,
}

impl<'a> Bnd4Record<'a> {
    pub fn name(&self) -> String {
        unsafe { magic::utf16le_by_vec_offset(&self.bnd.data, self.name_offset as i32) }
    }

    /// Direct reference to data (probably compressed)
    pub fn data(&self) -> &'a [u8] {
        let start = /*size_of::<Bnd4Header>() + */ self.data_offset as usize;
        let end = start + self.data_len as usize;
        return &self.bnd.data.as_slice()[start..end];
    }
}

pub struct IndexIter<'a> {
    index: &'a Bnd4<'a>,
    current_idx: i32,
}

impl<'a> Iterator for IndexIter<'a> {
    type Item = Bnd4Record<'a>;

    fn next(&mut self) -> Option<Self::Item> {
        self.current_idx += 1;

        if self.current_idx < self.index.header.record_count as i32 {
            let offset =
                size_of::<Bnd4Header>() + self.current_idx as usize * size_of::<Bnd4FileHeader>();

            let record = unsafe {
                (self.index.data.as_ptr().add(offset) as *const Bnd4FileHeader)
                    .as_ref()
                    .unwrap()
            };

            record.validate();

            return Some(Bnd4Record {
                bnd: self.index,
                data_offset: record.data_offset as u64,
                data_len: record.compressed_size as u64,
                name_offset: record.filename_offset,
            });
        }

        None
    }
}
