//! Formats definition, unpacker, converter

#![allow(unused)]

use std::time::{SystemTime, UNIX_EPOCH};

pub mod flver;

pub mod msb_2011;
pub mod msb_2015;

pub mod archive;
pub mod bdf3;
pub mod bhd5;
pub mod bhf3;
pub mod bnd_flags;
pub mod bnd3;
pub mod bnd4;
pub mod config;
pub mod dcx;
pub mod fhash;
pub mod loadlist;
pub mod magic;
pub mod mtd;
pub mod tpf;

/// Current time in millis. Just a convenience method
pub fn millis() -> u128 {
    SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .unwrap()
        .as_millis()
}

/// Alias on `[f32; 2]` representing Vector2 in game content
pub type Vec2 = [f32; 2];

/// Alias on `[f32; 3]` representing Vector3 in game content
pub type Vec3 = [f32; 3];

/// Debug trait
pub trait Validate {
    fn validate(&self);
}
