//! Own configuration: game type, game data location

use std::{fs::File, path::Path};

use serde::{Deserialize, Serialize};

pub const DS1_STEAM_LOCATION: &str =
    "C:\\Program Files (x86)\\Steam\\steamapps\\common\\Dark Souls Prepare To Die Edition\\DATA";

#[derive(Clone, Debug, Default, Serialize, Deserialize)]
pub struct UserConfig {
    /// Path to `DATA/` folder
    ///
    /// Defaults to [DS1_STEAM_LOCATION]
    pub game_root: String,

    /// Defaults to [GameType::DS1]
    pub game_type: GameType,

    /// .txt, one path per line
    pub archive_names: Option<String>,
}

impl UserConfig {
    pub fn file(&self, name: &str) -> Result<File, ()> {
        let root = self.game_root.clone();
        let path = Path::new(&root).join(name);
        let file = File::open(path).map_err(|_| ())?;

        Ok(file)
    }
}

/// Only DS1 is supported
#[derive(Clone, Debug, Default, PartialEq, Eq, Serialize, Deserialize)]
pub enum GameType {
    DeS,

    /// Maybe one day
    DeSR,

    #[default]
    DS1,

    DS1R,

    DS2,

    /// DS2 SotFS
    DS2S,

    /// Maybe one day
    BB,

    DS3,
}

pub fn get_user_config(path: &str) -> UserConfig {
    let cfg_path = Path::new(path);
    let mut cfg: UserConfig;

    if cfg_path.exists() {
        let data =
            std::fs::read_to_string(cfg_path).expect(&format!("{path} must be readable by app"));
        cfg = serde_json::from_str(&data).expect(&format!("{path} must be valid JSON config file"));
    } else {
        cfg = UserConfig::default();
        cfg.game_root = DS1_STEAM_LOCATION.into();
        cfg.archive_names = Some("./docs/res/filenames.txt".to_string());

        println!("No config found in {path}");

        let cfg_dir = Path::new(cfg_path.parent().unwrap());
        if !cfg_dir.exists() {
            std::fs::create_dir_all(cfg_dir).unwrap();
        }

        if !cfg_dir.is_dir() {
            panic!("{cfg_dir:?} must be a directory!")
        } else {
            let cfg_json = serde_json::to_string_pretty(&cfg).unwrap();
            std::fs::write(cfg_path, cfg_json)
                .expect(&format!("{path} must writeable readable by app"));
        }

        println!("Wrote default config to {path}");
    }

    if !Path::new(&cfg.game_root).exists() {
        panic!("Game root at {} not found", &cfg.game_root);
    }

    return cfg;
}
