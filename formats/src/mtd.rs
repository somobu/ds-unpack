//! A material definition format used in all souls games.
//!
//! Based mostly on [SoulsFormats](https://github.com/JKAnderson/SoulsFormats/blob/master/SoulsFormats)

use encoding_rs::SHIFT_JIS;

use crate::Validate;

pub struct Mtd<'a> {
    data: &'a [u8],
    pub shader_path: String,
    pub description: String,
    pub params: Vec<ParamEntry>,
    pub textures: Vec<TextureEntry>,
}

#[derive(Debug, PartialEq, Eq)]
pub struct ParamEntry {
    pub name: String,
    pub r#type: String,
    pub value: [u8; 16],
}

#[derive(Debug, PartialEq, Eq)]
pub struct TextureEntry {
    pub r#type: String,
    pub uv_number: u32,
    pub shader_data_idx: u32,
}

impl<'a> Mtd<'a> {
    pub fn new(data: &'a [u8]) -> Mtd<'a> {
        let mut ofs = 0;

        let file_header = unsafe { (data[..].as_ptr() as *const FileHeader).as_ref().unwrap() };
        file_header.validate();
        ofs += size_of_val(file_header);

        let mtd_header = unsafe { (data[ofs..].as_ptr() as *const Container).as_ref().unwrap() };
        mtd_header.validate();
        ofs += size_of_val(mtd_header);

        let data_header = unsafe {
            (data[ofs..].as_ptr() as *const DataHeader)
                .as_ref()
                .unwrap()
        };
        data_header.validate();
        ofs += size_of::<DataHeader>();

        let (shader_path, len) = marked_string(&data[ofs..], 0xA3).unwrap();
        ofs += len;

        let (description, len) = marked_string(&data[ofs..], 0x03).unwrap();
        ofs += len;

        let marker = unsafe { *(data.as_ptr().add(ofs) as *const u32) };
        assert_eq!(marker, 1);
        ofs += 4;

        let lists_header = unsafe {
            (data[ofs..].as_ptr() as *const ListsHeader)
                .as_ref()
                .unwrap()
        };
        lists_header.validate();
        ofs += size_of_val(lists_header);

        let marker = unsafe { *(data.as_ptr().add(ofs) as *const u32) };
        assert_eq!(marker, 0);
        ofs += 4;

        let marker = unsafe { *(data.as_ptr().add(ofs)) };
        assert_eq!(marker, 3);
        ofs += 4;

        let params_count = unsafe { *(data.as_ptr().add(ofs) as *const u32) };
        ofs += 4;

        let mut params: Vec<ParamEntry> = Vec::with_capacity(params_count as usize);
        for _ in 0..params_count {
            let (param_block, block_size) = parse_param_block(&data[ofs..]);
            ofs += block_size;
            params.push(param_block);
        }

        let marker = unsafe { *(data.as_ptr().add(ofs)) };
        assert_eq!(marker, 0x03);
        ofs += 4;

        let texture_count = unsafe { *(data.as_ptr().add(ofs) as *const u32) };
        ofs += 4;

        let mut textures: Vec<TextureEntry> = Vec::with_capacity(texture_count as usize);
        for _ in 0..texture_count {
            let (tex_block, block_size) = parse_texture_block(&data[ofs..]);
            ofs += block_size;
            textures.push(tex_block);
        }

        // End of textures block
        let marker = unsafe { *(data.as_ptr().add(ofs)) };
        assert_eq!(marker, 0x04);
        ofs += 4;

        let marker = unsafe { *(data.as_ptr().add(ofs) as *const u32) };
        assert_eq!(marker, 0);
        ofs += 4;

        // End of lists block
        let marker = unsafe { *(data.as_ptr().add(ofs)) };
        assert_eq!(marker, 0x04);
        ofs += 4;

        let marker = unsafe { *(data.as_ptr().add(ofs) as *const u32) };
        assert_eq!(marker, 0);
        ofs += 4;

        // End of data block
        let marker = unsafe { *(data.as_ptr().add(ofs)) };
        assert_eq!(marker, 0x04);
        ofs += 4;

        let marker = unsafe { *(data.as_ptr().add(ofs) as *const u32) };
        assert_eq!(marker, 0);
        // ofs += 4;

        // println!("{:02X?}", &data[ofs..(ofs + 16)]);

        Self {
            data,
            shader_path,
            description,
            params,
            textures,
        }
    }
}

fn parse_param_block(data: &[u8]) -> (ParamEntry, usize) {
    let mut ofs = 0;

    let param_block = unsafe {
        (data[ofs..].as_ptr() as *const ParamHeader)
            .as_ref()
            .unwrap()
    };
    param_block.validate();
    ofs += size_of_val(param_block);

    let name = marked_string(&data[ofs..], 0xA3).unwrap();
    ofs += name.1;

    let block_type = marked_string(&data[ofs..], 0x04).unwrap();
    ofs += block_type.1;

    let marker = unsafe { *(data.as_ptr().add(ofs) as *const u32) };
    assert_eq!(marker, 1);
    ofs += 4;

    let ugly_header = unsafe {
        (data[ofs..].as_ptr() as *const ParamValueHeader)
            .as_ref()
            .unwrap()
    };
    ugly_header.validate();
    ofs += size_of_val(ugly_header);

    // Fuck it, we gonna parse block_type by hand
    let _value_count = unsafe { *(data.as_ptr().add(ofs) as *const u32) };
    ofs += 4;

    let raw_value = match block_type.0.as_str() {
        "bool" => &data[ofs..ofs + 1],
        "int" => &data[ofs..ofs + 4],
        "int2" => &data[ofs..ofs + 4 * 2],
        "float" => &data[ofs..ofs + 4],
        "float2" => &data[ofs..ofs + 4 * 2],
        "float3" => &data[ofs..ofs + 4 * 3],
        "float4" => &data[ofs..ofs + 4 * 4],
        _ => todo!("{}", block_type.0),
    };
    ofs += raw_value.len();

    let marker = data[ofs];
    assert_eq!(marker, 0x04);
    ofs += 1;
    ofs = ceil_offset(ofs);

    // Actually it is not marker, it is padding
    let marker = unsafe { *(data.as_ptr().add(ofs) as *const u32) };
    assert_eq!(marker, 0);
    ofs += 4;

    let mut value = [0_u8; 16];
    value[0..raw_value.len()].copy_from_slice(raw_value);

    return (
        ParamEntry {
            name: name.0,
            r#type: block_type.0,
            value,
        },
        ofs,
    );
}

fn parse_texture_block(data: &[u8]) -> (TextureEntry, usize) {
    let mut ofs = 0;

    let texture_block = unsafe { (data[ofs..].as_ptr() as *const TexHeader).as_ref().unwrap() };
    texture_block.validate();
    ofs += size_of_val(texture_block) as usize;

    let tex_type = marked_string(&data[ofs..], 0x35).unwrap();
    ofs += tex_type.1;

    let uv_no = unsafe { *(data.as_ptr().add(ofs) as *const u32) };
    ofs += 4;

    let marker = data[ofs];
    assert_eq!(marker, 0x35);
    ofs += 4;

    let shader_data_idx = unsafe { *(data.as_ptr().add(ofs) as *const u32) };
    ofs += 4;

    (
        TextureEntry {
            r#type: tex_type.0,
            uv_number: uv_no,
            shader_data_idx,
        },
        ofs,
    )
}

/// Returns how much bytes has to be skipped
fn marked_string<'a>(data: &'a [u8], end_marker: u8) -> Option<(String, usize)> {
    let len = unsafe { *(data.as_ptr() as *const u32) } as usize;

    let bytes: &'a [u8] = &data[4..(4 + len)];
    let marker = unsafe { *(data.as_ptr().add(4 + len) as *const u8) };
    assert_eq!(end_marker, marker);

    let (s, _, _) = SHIFT_JIS.decode(bytes);

    let ofs = 4 + len + 1; // add marker
    let ofs = ceil_offset(ofs); // Align to u32
    Some((s.to_string(), ofs))
}

fn ceil_offset(ofs: usize) -> usize {
    ((ofs + 3) / 4) * 4
}

pub type FileHeader = Block<0, 3, 0x01>;
pub type ContainerHeader = Block<1, 2, 0xB0>;
pub type DataHeader = Block<2, 4, 0xA3>;
pub type ListsHeader = Block<3, 4, 0xA3>;
pub type ParamHeader = Block<4, 4, 0xA3>;
pub type ParamValueHeader = Block<0, 1, 0>;
pub type TexHeader = Block<0x2000, 3, 0xA3>;

#[repr(C)]
pub struct Block<const TYPE: u32, const VERSION: u32, const END_MARKER: u32> {
    padding: u32,

    /// PLEASE do not trust this field
    length: u32,

    r#type: u32,
    version: u32,

    /// Wtf?! There's can be non-zero data in end_marker[1..4]!
    end_marker: [u8; 4],
}

impl<const TYPE: u32, const VERSION: u32, const END_MARKER: u32> Block<TYPE, VERSION, END_MARKER> {
    /// It's a lie! A lie!
    fn len_from_start(&self) -> usize {
        self.length as usize + 8
    }
}

impl<const TYPE: u32, const VERSION: u32, const END_MARKER: u32> Validate
    for Block<TYPE, VERSION, END_MARKER>
{
    fn validate(&self) {
        assert_eq!(self.padding, 0);

        if TYPE != 0 {
            assert_eq!(self.r#type, TYPE);
        }

        if VERSION != 0 {
            assert_eq!(self.version, VERSION);
        }

        if END_MARKER != 0 {
            assert_eq!(self.end_marker[0], END_MARKER as u8);
        }
    }
}

#[repr(C)]
pub struct Container {
    header: ContainerHeader,

    string_len: u32,
    string_data: [u8; 4],
    string_end: [u8; 4],

    thousand: u32,

    /// Wtf?! There's can be non-zero in end_marker[1..4]!
    one: [u8; 4],
}

const MTD: [u8; 4] = *b"MTD ";

impl Validate for Container {
    fn validate(&self) {
        self.header.validate();
        assert_eq!(self.string_len, 4);
        assert_eq!(self.string_data, *b"MTD ");
        assert_eq!(self.string_end[0], 0x34);
        assert_eq!(self.thousand, 1000);
        assert_eq!(self.one[0], 1);
    }
}
