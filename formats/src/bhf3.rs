//! Binder Header File, ver 3

use std::{mem::size_of, slice};

use crate::{magic, Validate};

const MAGIC_HEADER: [u8; 4] = *b"BHF3";

const DS1_BHF_ID: [u8; 4] = *b"07D7";

#[repr(C)]
pub struct Bhf3Header {
    /// Should be "BHF3"
    format: [u8; 4],

    id: [u8; 4],
    version: i32,

    // Flags?
    unknown: [u8; 4],

    record_count: i32,

    padding: [u32; 3],
}

impl Validate for Bhf3Header {
    fn validate(&self) {
        assert_eq!(self.format, MAGIC_HEADER);
        assert_eq!(self.id, DS1_BHF_ID);
        assert_eq!(self.padding, [0u32; 3]);
    }
}

#[derive(Debug)]
#[repr(C)]
pub struct Record {
    pub flags: u8,
    padding: [u8; 3],

    pub compressed_size: i32,

    pub offset: u32,

    pub id: i32,

    pub filename_offset: i32,

    pub redundant_size: i32,
}

impl Validate for Record {
    fn validate(&self) {
        assert_eq!(self.padding, [0u8; 3]);
        assert_eq!(self.compressed_size, self.redundant_size);
    }
}

pub struct Bhf3<'a> {
    pub data: Vec<u8>,

    pub header: &'a Bhf3Header,
    pub records: &'a [Record],
}

impl<'a> Bhf3<'a> {
    pub fn new(data: Vec<u8>) -> Bhf3<'a> {
        let pointer = data.as_ptr();
        let header = unsafe { (pointer as *const Bhf3Header).as_ref().unwrap() };
        header.validate();

        let pointer = unsafe { pointer.add(size_of::<Bhf3Header>()) };
        let records = unsafe {
            slice::from_raw_parts(pointer as *const Record, header.record_count as usize)
        };

        Bhf3 {
            data,
            header,
            records,
        }
    }

    pub fn record_name_by_offset(&self, offset: i32) -> &str {
        unsafe { magic::ascii_by_vec_offset(&self.data, offset) }
    }

    pub fn record_filename_only(&self, offset: i32) -> &str {
        let longname = self.record_name_by_offset(offset);
        let last_shash = longname.rfind("\\").unwrap() + 1;
        let dot = longname.find(".").unwrap();
        &longname[last_shash..dot]
    }
}
