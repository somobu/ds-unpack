//! Utilities - obscure programming things

use std::{
    ffi::CStr,
    fs::File,
    io::{self, BufReader, BufWriter, Read, Seek, SeekFrom, Write},
};

/// Converts any structure pointer to [u8] pointer
pub unsafe fn to_u8_slice<T: Sized>(p: &mut T) -> &mut [u8] {
    ::core::slice::from_raw_parts_mut((p as *mut T) as *mut u8, ::core::mem::size_of::<T>())
}

pub fn dump_stream<R: Read>(from: &mut R, to: &str) -> Result<(), ()> {
    let file = File::create(to).map_err(|_| ())?;
    let mut buf_writer = BufWriter::new(file);

    let mut raw_buffer = [0u8; 8128];
    let buffer = raw_buffer.as_mut_slice();

    loop {
        let len = from.read(buffer).map_err(|_| ())?;

        if len == 0 {
            break;
        }

        buf_writer.write(buffer).map_err(|_| ())?;
    }

    Ok(())
}

pub fn dump_vec(from: &[u8], to_file: &str) -> Result<(), ()> {
    let mut file = File::create(to_file).map_err(|_| ())?;
    file.write_all(&from).map_err(|_| ())?;

    Ok(())
}

pub unsafe fn ascii_by_vec_offset(data: &Vec<u8>, offset: i32) -> &str {
    let start = data.as_ptr().add(offset as usize);

    let s = CStr::from_ptr(start as *const i8);
    s.to_str().ok().unwrap()
}

pub unsafe fn utf16le_by_vec_offset(data: &Vec<u8>, offset: i32) -> String {
    let offset = offset as usize;
    let mut count = 0;

    while data[offset + count] != 0 {
        count += 2;
    }

    encoding_rs::UTF_16LE
        .decode(&data[offset..offset + count])
        .0
        .to_string()
}

pub unsafe fn ascii_by_relative_offset<'a, T>(base: &'a T, offset: i32) -> Option<&'a str> {
    let entry_start = (base as *const T) as *const u8;
    let start = entry_start.add(offset as usize);

    let s = CStr::from_ptr(start as *const i8);
    s.to_str().ok()
}

pub trait BetterSeek: Seek {
    /// Offset reader from current position
    fn seek_by(&mut self, pos: i64) -> io::Result<u64> {
        self.seek(SeekFrom::Current(pos))
    }

    /// Offset reader from start (positive value) or and (negative)
    fn seek_to(&mut self, pos: i64) -> io::Result<u64> {
        if pos >= 0 {
            self.seek(SeekFrom::Start(pos as u64))
        } else {
            self.seek(SeekFrom::End(pos))
        }
    }

    fn position(&mut self) -> io::Result<u64> {
        self.seek(SeekFrom::Current(0))
    }
}

impl BetterSeek for File {}
impl<R: ?Sized + Seek> BetterSeek for BufReader<R> {}
impl<R: Sized + AsRef<[u8]>> BetterSeek for std::io::Cursor<R> {}
