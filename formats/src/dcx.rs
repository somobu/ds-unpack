//! Compressed (zlib/deflate) container. Extension: .dcx

use std::{io::Read, mem::size_of};

use crate::Validate;

pub const MAGIC_HEADER: [u8; 4] = *b"DCX\0";

/// PS3 had 256 mbytes of RAM, so 2x128 mbytes should be enough for unpacker
pub const ZLIB_CAPACITY: usize = 128 * 1024 * 1024;

#[repr(C)]
pub struct DcxHeader {
    /// Should be "DCX\0"
    magic_header: [u8; 4],

    unknown_1: i32,

    dcs_offset: i32,
    dcp_offset: i32,
    redundant_dcp_offset: i32,

    dcs_header_size: i32,

    // DCS header below?
    /// Should be "DCS\0"
    dcs_signature: [u8; 4],
    uncompressed_size: i32,
    compressed_size: i32,

    /// Should be "DCP\0"
    dcp_signature: [u8; 4],

    /// Should be "DFLT"
    dcp_method: [u8; 4],
    dca_offset: i32,
    compression_level: i32,
    padding: [u8; 12],

    zlib_ver: i32,

    // DCA header below?
    /// Should be "DCA\0"
    dca_signature: [u8; 4],
    dca_header_size: i32,
}

impl Validate for DcxHeader {
    fn validate(&self) {
        assert_eq!(self.magic_header, MAGIC_HEADER);
        assert_eq!(self.unknown_1, 1 << 8);
        assert_eq!(self.dcs_signature, "DCS\0".as_bytes());
        assert_eq!(self.dcp_signature, "DCP\0".as_bytes());
        assert_eq!(self.dcp_method, "DFLT".as_bytes());
        assert_eq!(self.compression_level, 9);
        assert_eq!(self.dca_signature, "DCA\0".as_bytes());
    }
}

pub fn is_dcx(magic: &[u8]) -> bool {
    *magic == MAGIC_HEADER
}

pub fn decompress(data: &[u8]) -> Vec<u8> {
    let header = unsafe { (data.as_ptr() as *const DcxHeader).as_ref().unwrap() };
    header.validate();

    let data = &data[size_of::<DcxHeader>()..];
    let is_zlib = data[0] == 0x78 && (data[1] == 0xda);

    if is_zlib {
        let mut zliber = flate2::read::ZlibDecoder::new(data);

        let mut result = Vec::new();
        zliber.read_to_end(&mut result).unwrap();

        result
    } else {
        todo!("unknown compression algo")
    }
}

pub fn decompress_file(data: &[u8]) -> Vec<u8> {
    let header_len = size_of::<DcxHeader>();
    let header_data = data[0..header_len].to_vec();

    let header = unsafe { (header_data.as_ptr() as *const DcxHeader).as_ref().unwrap() };
    header.validate();

    let first_bytes = &data[header_len..(header_len + 2)];

    let is_zlib = first_bytes[0] == 0x78 && (first_bytes[1] == 0xda);
    if is_zlib {
        let mut zliber = flate2::read::ZlibDecoder::new(&data[header_len..]);

        let mut result = Vec::new();
        zliber.read_to_end(&mut result).unwrap();

        result
    } else {
        todo!("unknown compression algo")
    }
}
