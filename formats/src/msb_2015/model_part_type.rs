//! Enum decribing model or part type
//! 
//! Supposedly, models and parts share the same type def

/// Model type enum
#[derive(Debug, PartialEq, Eq)]
#[repr(u32)]
pub enum ModelPartType {
    /// Model for static piece of visual map geometry -- terrain and scenery.
    /// 
    /// Located at /map/m`##`_nn_00_00/`NAME`A`##`.flver.dcx. Name format: `mXXXXBX`
    MapPiece = 0,

    /// Model for a dynamic or interactible part
    /// 
    /// Located at /obj/`NAME`.objbnd. Name format: `oXXXX`
    Object = 1,

    /// Model for non-player entity
    /// 
    /// Located at /chr/`NAME`.chrbnd and /chr/`NAME`.anibnd. Name format: `cXXXX`
    Enemy = 2,

    /// Model for player spawn point (?)
    /// 
    /// Located at /chr/`NAME`.chrbnd and /chr/`NAME`.anibnd. Name format: `c0000`
    Player = 4,

    /// Model for physics collision
    /// 
    /// Located at /map/m`##`_nn_00_00/`NAME`A`##`.hkx. Name format: `hXXXXBX`
    Collision = 5,

    /// Model for AI navigation mesh
    /// 
    /// Located at ???. Maybe in `nvmbnd`?. Name format: `nXXXXBX`
    Navmesh = 6,

    /// Objects that don't appear normally; either unused, or used for cutscenes
    DummyObject = 9,

    /// NPCs that don't appear normally; either unused, or used for cutscenes
    DummyEnemy = 10,

    /// Dummy parts that reference an actual collision and cause it to load another map
    /// 
    /// Located at /map/m`##`_nn_00_00/`NAME`A`##`.hkx (?)
    ConnectCollision = 11,
}
