//! A map layout file used in BB. Extension: .msb
//!
//! File spec and comments taken from [SoulsFormats](https://github.com/JKAnderson/SoulsFormats/)

use std::{marker::PhantomData, mem::size_of};

use group::Group;

use crate::Validate;

use self::{
    event::EventEntry, group::MsbGroup, model::ModelEntry, part::PartEntry, region::RegionEntry,
};

pub mod event;
pub mod group;
pub mod model;
pub mod model_part_type;
pub mod part;
pub mod region;

/// Struct to handle MSB data
pub struct Msb2015<'a> {
    pub(self) data: Vec<u8>,

    pub header: &'a Header,

    model_offset: i32,
    event_offset: i32,
    point_offset: i32,
    parts_offset: i32,
}

impl<'a> Msb2015<'a> {
    pub fn new(data: Vec<u8>) -> Msb2015<'a> {
        let mut pointer = data.as_ptr();

        let header = unsafe { (pointer as *const Header).as_ref().unwrap() };
        header.validate();

        println!("Header is good");

        let mut msb = Msb2015 {
            data,
            header,
            model_offset: 0,
            event_offset: 0,
            point_offset: 0,
            parts_offset: 0,
        };

        let l = {
            let model = msb.group_by_offset::<ModelEntry>(size_of::<Header>() as i32);
            assert_eq!(model.name(), "MODEL_PARAM_ST");

            let event = model.next::<EventEntry>();
            assert_eq!(event.name(), "EVENT_PARAM_ST");

            let point = event.next::<RegionEntry>();
            assert_eq!(point.name(), "POINT_PARAM_ST");

            let parts = point.next::<PartEntry>();
            assert_eq!(parts.name(), "PARTS_PARAM_ST");

            (event.offset, point.offset, parts.offset)
        };

        msb.event_offset = l.0;
        msb.point_offset = l.1;
        msb.parts_offset = l.2;

        msb
    }

    /// Model files that are available for parts to use.
    pub fn models(&self) -> MsbGroup<ModelEntry> {
        self.group_by_offset(self.model_offset)
    }

    /// Dynamic or interactive systems such as item pickups, levers, enemy spawners, etc.
    pub fn events(&self) -> MsbGroup<EventEntry> {
        self.group_by_offset(self.event_offset)
    }

    /// Points or areas of space that trigger some sort of behavior.
    pub fn regions(&self) -> MsbGroup<RegionEntry> {
        self.group_by_offset(self.point_offset)
    }

    /// Instances of actual things in the map.
    pub fn parts(&self) -> MsbGroup<PartEntry> {
        self.group_by_offset(self.parts_offset)
    }

    fn group_by_offset<T: Validate>(&self, offset: i32) -> MsbGroup<T> {
        assert!(offset >= 0);

        let offset = offset as usize;
        assert!(offset < (self.data.len() - size_of::<Group>()));

        let group_ptr = unsafe { (self.data.as_ptr() as *const u8).add(offset) };
        let group = unsafe { (group_ptr as *const Group).as_ref().unwrap() };

        group.validate();

        return MsbGroup::<T> {
            offset: offset as i32,
            msb: self,
            group,
            d: PhantomData,
        };
    }

    fn entry_by_offset<T: Validate>(&self, offset: i32) -> &T {
        assert!(offset >= 0);

        let offset = offset as usize;
        assert!(offset < (self.data.len() - size_of::<T>()));

        let entry_ptr = unsafe { (self.data.as_ptr() as *const u8).add(offset) };
        let entry = unsafe { (entry_ptr as *const T).as_ref().unwrap() };

        entry.validate();

        return entry;
    }
}

#[repr(C)]
pub struct Header {
    maigc: [u8; 4],
    unknown_1: i32,
    unknown_2: i32,
    big_endian: bool,
    bit_big_endian: bool,
    text_encoding: u8,
    is_64bit_offset: u8,
}

impl Validate for Header {
    fn validate(&self) {
        assert_eq!(self.maigc, *b"MSB ");
        assert_eq!(self.unknown_1, 1);
        assert_eq!(self.unknown_2, 0x10);
        assert_eq!(self.big_endian, false);
        assert_eq!(self.bit_big_endian, false);
        assert_eq!(self.text_encoding, 1);
        assert_eq!(self.is_64bit_offset, 0xFF);
    }
}
