//! Group - map event

use crate::Validate;

#[repr(C)]
pub struct EventEntry {
    name_offset: i32,

    event_id: i32,
    entry_type: u32,
    id: i32,
    base_data_offset: i32,
    type_data_offset: i32,
    padding: i32,
}

impl Validate for EventEntry {
    fn validate(&self) {
        assert!(self.padding == 0);
    }
}
