//! MSB group description

use std::marker::PhantomData;

use crate::Validate;

use super::Msb2015;

/// In-memory group representation
#[repr(C)]
pub struct Group {
    zero: i32, // Zero indeed
    pub(self) name_offset: i32,
    pub(self) entry_count: i32,
    pub(self) entry_offsets: i32, // Actually it is array [i32; entry_count]
}

impl Group {
    pub fn validate(&self) {
        assert!(self.zero == 0);
    }

    fn offset(&self, offset_index: i32) -> i32 {
        assert!(offset_index >= 0);
        assert!(offset_index < self.entry_count);
        unsafe { *(&self.entry_offsets as *const i32).add(offset_index as usize) }
    }

    fn next_group_offset(&self) -> i32 {
        self.offset(self.entry_count - 1)
    }
}

pub struct MsbGroup<'a, T: Validate> {
    pub(super) offset: i32,
    pub(super) msb: &'a Msb2015<'a>,
    pub(super) group: &'a Group,
    pub(super) d: PhantomData<T>,
}

impl<'a, T: Validate> MsbGroup<'a, T> {
    pub fn name(&self) -> &str {
        let offset = self.group.name_offset;
        let start = offset as usize;
        let mut end = offset as usize;
        while self.msb.data.as_slice()[end] != 0 {
            end += 1;
        }

        std::str::from_utf8(&self.msb.data.as_slice()[start..end]).unwrap()
    }

    pub fn entry_count(&self) -> usize {
        self.group.entry_count as usize - 1
    }

    pub fn entry_by_idx(&self, idx: usize) -> &T {
        let idx = idx as i32;
        let offset = self.group.offset(idx);
        let t = self.msb.entry_by_offset::<T>(offset);
        t.validate();
        t
    }

    pub fn is_first(&self) -> bool {
        self.offset == 0
    }

    pub fn is_last(&self) -> bool {
        self.group.next_group_offset() == 0
    }

    pub fn next<D>(&self) -> MsbGroup<'a, D>
    where
        D: Validate,
    {
        self.msb.group_by_offset(self.group.next_group_offset())
    }
}
