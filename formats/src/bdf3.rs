//! Binder Data File, ver 3
//!
//! Container for compressed (DCX) or uncompressed (BHF) data

use serde::Serialize;

use crate::Validate;

const MAGIC_HEADER: [u8; 4] = *b"BDF3";
const DS1_BDF_ID: [u8; 4] = *b"07D7";

#[derive(Serialize)]
#[repr(C)]
pub struct Bdf3Header {
    /// Should be "BDF3"
    magic_header: [u8; 4],

    id: [u8; 4],
    version: i32,

    padding: i32,
}

impl Validate for Bdf3Header {
    fn validate(&self) {
        assert_eq!(self.magic_header, MAGIC_HEADER);
        assert_eq!(self.id, DS1_BDF_ID);
        assert_eq!(self.padding, 0);
    }
}
