//! Maps enumeration. Extension: .loadlist, .loadlistlist

use encoding_rs::SHIFT_JIS;

#[derive(Debug, Default)]
pub struct Entry {
    pub path: String,
    pub name: String,
}

impl Entry {
    pub fn correct_map_path(&self) -> String {
        assert!(self.path.starts_with("map:"));
        self.path.replace("map:/", "/map/")
    }

    /// Map file name w/o extension and path
    ///
    /// Ex.: `map:/MapStudio/m14_00_00_00.msb` becomes `m14_00_00_00`
    pub fn map_basename(&self) -> String {
        assert_eq!(&self.path[0..15], "map:/MapStudio/");
        assert_eq!(&self.path[27..31], ".msb");

        self.path[15..27].to_string()
    }
}

pub fn from(buf: Vec<u8>) -> Vec<Entry> {
    let mut rz = Vec::<Entry>::new();

    let mut entry = Entry::default();

    let mut start_ptr = 0;
    let mut end_ptr = 0;
    let l = buf.len();

    while end_ptr < l {
        let mut divider_ptr = 0;

        // Skip to CR
        while end_ptr < (l - 2) && buf[end_ptr] != b'\r' {
            end_ptr += 1;

            if buf[end_ptr] == 0x20 && divider_ptr == 0 {
                divider_ptr = end_ptr;
            }
        }

        if divider_ptr != 0 {
            let path = &buf.as_slice()[start_ptr..divider_ptr];
            let path = std::str::from_utf8(path).unwrap().to_string();
            let (name, _, _) = SHIFT_JIS.decode(&buf.as_slice()[(divider_ptr + 2)..end_ptr]);
            let name = name.to_string();
            rz.push(Entry { path, name });
        }

        assert_eq!(buf[end_ptr + 0], b'\r');
        assert_eq!(buf[end_ptr + 1], b'\n');
        end_ptr += 2;
        start_ptr = end_ptr;
    }

    return rz;
}
