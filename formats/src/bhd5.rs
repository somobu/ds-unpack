//! Binder Header Data, ver 5
//!
//! Single entry of .bhd5 file
//!
//! Based mostly on [SoulsFormats](https://github.com/JKAnderson/SoulsFormats/blob/master/SoulsFormats)

use std::mem::size_of;

use memmap2::Mmap;

use crate::Validate;

pub const MAGIC_HEADER: [u8; 4] = *b"BHD5";

/// TODO: DS2 (and newer) uses slightly different header format
#[repr(C)]
pub struct Bhd5Header {
    /// Should be BHD5
    format: [u8; 4],
    is_little_endian: bool,
    unknown5: bool,

    padding: [u8; 2],

    /// Should be 1
    unknown_3: i32,

    pub file_size: u32,
    pub bucket_count: u32,
    pub buckets_offset: u32,
}

impl Validate for Bhd5Header {
    fn validate(&self) {
        assert_eq!(self.format, MAGIC_HEADER);
        assert_eq!(self.is_little_endian, true); // TODO: invert check on BE systems
        assert_eq!(self.padding, [0, 0]);
        assert_eq!(self.unknown_3, 1);
        assert!(self.buckets_offset >= ::core::mem::size_of::<Bhd5Header>() as u32);
    }
}

#[repr(C)]
pub struct BucketHeader {
    pub header_count: u32,

    /// Somobu: I assume this is absolute offset of header in BHD5
    pub header_offset: u32,
}

/// TODO: DS2+ has more fields
/// TODO: DS3 adds some more
#[derive(Debug)]
#[repr(C)]
pub struct Record {
    /// Hash of the full file path using From's algorithm
    pub file_name_hash: u32,

    /// Full size of the file data in the BDT, unpadded
    ///
    /// **Warning** use `get_padded_file_size` to get padded size
    file_size: u32,

    /// Beginning of file data in the BDT
    pub file_offset: u64,
}

impl Record {
    pub fn get_padded_file_size(&self) -> u32 {
        if self.file_size % 16 == 0 {
            return self.file_size;
        } else {
            return ((self.file_size / 16) + 1) * 16;
        }
    }

    pub fn describe(&self) -> String {
        format!(
            "Record 0x{:X} at 0x{:X}, {:#} Kb",
            self.file_name_hash,
            self.file_offset,
            self.file_size / (1024)
        )
    }
}

pub struct Bhd5<'a> {
    data: Mmap,
    pub header: &'a Bhd5Header,
}

impl<'a> Bhd5<'a> {
    pub fn new(data: Mmap) -> Bhd5<'a> {
        let header = unsafe { (data.as_ptr() as *const Bhd5Header).as_ref().unwrap() };
        header.validate();

        Bhd5 { header, data }
    }

    fn get_bucket(&self, bucket_idx: u32) -> &BucketHeader {
        debug_assert!(bucket_idx < self.header.bucket_count);

        let offset = size_of::<Bhd5Header>() + bucket_idx as usize * size_of::<BucketHeader>();

        unsafe {
            (self.data.as_ptr().offset(offset as isize) as *const BucketHeader)
                .as_ref()
                .unwrap()
        }
    }

    fn get_record_by_offset(&self, base_offset: u32, record_idx: u32) -> &Record {
        let offset = base_offset + record_idx * (size_of::<Record>() as u32);

        unsafe {
            (self.data.as_ptr().offset(offset as isize) as *const Record)
                .as_ref()
                .unwrap()
        }
    }

    pub fn get_record_by_hash(&self, name_hash: u32) -> Option<&Record> {
        let bucket_idx = name_hash % self.header.bucket_count;
        let bucket = self.get_bucket(bucket_idx);

        for i in 0..bucket.header_count {
            let record = self.get_record_by_offset(bucket.header_offset, i);
            if record.file_name_hash == name_hash {
                return Some(record);
            }
        }

        return None;
    }

    pub fn record_iter<'b>(&'b self) -> IndexIter<'b> {
        IndexIter {
            index: self,
            current_bucket_idx: 0,
            current_record_idx: -1,
        }
    }
}

pub struct IndexIter<'a> {
    index: &'a Bhd5<'a>,
    current_bucket_idx: u32,
    current_record_idx: i32,
}

impl<'a> Iterator for IndexIter<'a> {
    type Item = &'a Record;

    fn next(&mut self) -> Option<Self::Item> {
        self.current_record_idx += 1;

        let mut bucket = self.index.get_bucket(self.current_bucket_idx);

        while self.current_record_idx >= bucket.header_count as i32 {
            self.current_bucket_idx += 1;
            self.current_record_idx = 0;

            if self.current_bucket_idx >= self.index.header.bucket_count {
                return None;
            } else {
                bucket = self.index.get_bucket(self.current_bucket_idx);
            }
        }

        return Some(
            self.index
                .get_record_by_offset(bucket.header_offset, self.current_record_idx as u32),
        );
    }
}
