#!/bin/bash
set -euxo pipefail

cd book
mdbook serve --open
cd ..

