# Research projects list

## Known

Projects used as reference

| Name                | Note                                | Ref                                                       |
|---------------------|-------------------------------------|-----------------------------------------------------------|
| Souls Formats       | Base **SoulsFormats** repo <3       | [GitHub](https://github.com/JKAnderson/SoulsFormats)      |
| DS Tools            | SoulsFormats w/ changes and fixes   | [GitHub](https://github.com/katalash/dstools/)            |
| ds_extract_and_pack | BND/DCX/TPF info                    | [GitHub](https://github.com/MasonM/ds_extract_and_pack)   |
| Blender FLVER       | Flver converter                     | [GitHub](https://github.com/kotn3l/blender-flver)         |



https://files.dece.space/docs/darksoulsdev/darksoulsdev.shgck.io/index.php/Main_Page.html

## Seen

Projects that may come useful later, sorted by author's name

| Name              | Scope          | Ref                                                               |
|-------------------|----------------|-------------------------------------------------------------------|
| FLVER editor      |                | https://github.com/asasasasasbc/FLVER_Editor                      |
| DS2+ unpacker     | DS2,DS3        | https://github.com/Atvaark/BinderTool                             |
| DS2 formats       | DS2            | https://github.com/Atvaark/DarkSoulsII.FileFormats                |
| DS3 formats       | DS3            | https://github.com/Atvaark/DarkSoulsIII.FileFormats               |
| DS1-2 viewer      | DS1            | https://github.com/colevk/dark-souls-map-viewer                   |
| HKX noesis plugin |                | https://github.com/Danilodum/dark_souls_hkx                       |
| DS3 server        |                | https://github.com/garyttierney/ds3-open-re                       |
| Some tools        |                | https://github.com/gibbed/Gibbed.DarkSouls                        |
| Cleaned AI LUAs   |                | https://github.com/Grimrukh/SoulsAI                               |
| DS unpack+edit    | DS1:Re         | https://github.com/Grimrukh/soulstruct                            |
| UDSFM             | DS1            | https://github.com/HotPocketRemix/UnpackDarkSoulsForModding       |
| Texture extract   |                | https://github.com/HotPocketRemix/DarkSoulsTextureExtractor       |
| Event scripts     |                | https://github.com/HotPocketRemix/DSEventScriptTools              |
| Texture repacker  |                | https://github.com/JKAnderson/DSR-TPUP                            |
| DS lua decompiler |                | https://github.com/katalash/DSLuaDecompiler                       |
| Anim editor       |                | https://github.com/loosepolygon/DarkSoulsAnimTool                 |
| Lua decoder       |                | https://github.com/loosepolygon/luadec-5.0                        |
| TAE editor        | DS, BB, DS3, S | https://github.com/Meowmaritus/DSAnimStudio                       |
| Text viewer       |                | https://github.com/mrexodia/DarkSouls3.TextViewer                 |
| UDSFM C#          |                | https://github.com/Nordgaren/Unpack-Dark-Souls-For-Modding-CSharp | 
| DS1-3 unpacker    |                | https://github.com/Nordgaren/UXM-Selective-Unpack                 |
| SL2 unpacker      |                | https://github.com/pawREP/Dark-Souls-Remastered-SL2-Unpacker      |
| Universal editor  |                | https://github.com/Philiquaz/DSParamStudio                        |
| DS3 param extract |                | https://github.com/Pireax/DS3ParamExtractor                       |
| HavocLib          |                | https://github.com/PredatorCZ/HavokLib                            |
| DS Game file lib  |                | https://github.com/SeanPesce/Dark-Souls-Game-Files-Lib            |
| DeS+ map editor:  |                | https://github.com/soulsmods/DSMapStudio                          |
| Behbnd editor     |                | https://github.com/The12thAvenger/DS3BehaviorTool                 |
| DS3 docs          |                | https://github.com/vawser/DS3-Documentation                       |
| DeS-BNDBuild      | DeS            | https://github.com/Wulf2k/DeS-BNDBuild                            |



