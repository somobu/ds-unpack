# File formats reference

Based on: [Ds2.FileFormats](https://github.com/Atvaark/DarkSoulsII.FileFormats/blob/master/README.md)

## Archive file formats

Name       | Description
---------- | -----------
.anibnd    | Animation archive
.bdt       | General purpose archive (headerless)
.bhd       | General purpose archive header
.bnd       | General purpose archive
.commonbnd | General purpose archive
.dclbnd    | Decal archive
.dcx       | Compressed file archive
.envbnd    | Map environment archive
.febnd     | Frontend archive
.fetexbnd  | Frontend texture archive
.ffxbnd    | SFX archive
.fltbnd    | Filter archive
.fontbnd   | Font archive
.gibhd     | Model archive header
.hkxbhd    | Havok Engine archive header
.hkxbdt    | Havok Engine archive (headless?)
.mapbhd    | Map archive header
.nvmbnd    | Navmesh (?) archive
.remobnd   | Cutscene archive
.texbnd    | Texture archive
.tpfbdt    | Texture archive (headerless)
.tpfbhd    | Texture archive header

## File formats

Name       | Description
---------- | ------------
.acb       | Unknown (Map model/texture related)
.bin       | Word filter
.bmp       | Bitmap
.breakobj  | Destructible map objects
.btao      | Unknown (Map/Light related)
.btl       | Unknown (Map related)
.btof      | Unknown (Map/Light related)
.btpb      | Unknown (Map/Point could related)
.btpo      | Unknown (Map/Light related)
.ccm       | Font glyph definition file (sans the font bitmap) 
.clm       | Unknown (Cloth related)
.ctl       | FaceGen Control
.dds       | Direct Draw Surface
.drb       | Icon (?)
.egt       | FaceGen Statistical Texture
.emevd     | Special effect table
.esd       | AI state machine
.fev       | Audio
.fltparam  | Filter settings
.flvpwv    | Unknown (Model related)
.fmg       | String table
.fpo       | Fragment shader
.fsb       | Sound
.fxo       | HLSLShader
.hkt       | Unknown (Havok/Model related)
.hkx       | Unknown (Havok related)
.hkxpwv    | Unknown (Havok related)
.ini       | Settings
.itl       | Sound
.list      | Map id and name table
.mcg       | Navigation graph ([ref](https://github.com/Grimrukh/soulstruct-blender))
.mcp       | Navigation data ([ref](https://github.com/soulsmods/DSMapStudio/blob/master/src/Andre/SoulsFormats/SoulsFormats/Formats/MCP.cs#L10))
.msb       | Map layout ([explained](https://github.com/katalash/dstools/blob/master/README.md#msb-basics))
.mtd       | Material
.mte       | Unknown (Map related)
.mte       | Unknown (Model related)
.ngp       | Unknown (Map related)
.nmb       | Unknown (Animation related)
.nsa       | Animation
.nvm       | Navmesh ([ref](https://github.com/Grimrukh/soulstruct-blender))
.param     | General purpose lookup-table
.pem       | RSA public key container
.pfbbin    | Unknown (Model related)
.pxt       | Unknown (Texture related)
.remo      | Cutscene
.sib       | Unknown (Model related) 
.sfxparam  | Sound settings
.tae       | Animation listings and events
.tga       | Truevision TGA
.tpf       | Texture
.vpo       | Vertex shader
.vsd       | Voice synchronization
.xml       | Shader information
.xpu       | GUI HLSL Shader
.xvu       | GUI HLSL Shader
