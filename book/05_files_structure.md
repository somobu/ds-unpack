# Content structure

Relevant to DS1 only.

## Files on disk

Overview of known folders and files

`#` - some number or number range

| Path                         | File                        | Format             | Use/content                       |
|------------------------------|-----------------------------|--------------------|-----------------------------------|
| %steam%/%game%/DATA/         | DARKSOULS.exe               | Windows executable | -                                 |
|                              | dvdbnd0.bdt - dvdbnd3.bdt   | List of BDF3s      | "Binder" container data           |
|                              | dvdbnd0.bhd5 - dvdbnd3.bhd5 | BHD5               | "Binder" container header (index) |
|                              | fmod_event.dll              | Windows shared lib | -                                 |
|                              | fmodex.dll                  | Windows shared lib | -                                 |
|                              | steam_api.dll               | Windows shared lib | -                                 | 
| %appdata%/Local/NBGI/%game%/ | DarkSouls.ini               | INI config file    | Game config?                      |
| %docs%/NBGI/%game%/#/        | DRAKS000#.sl2               | ???                | Save file                         |


### Game archives contents

It seems that game data was split between archives based on its type:

```
Archive 0. Levels + char
Archive 1. Parts + sounds
Archive 2. Events + scripts
Archive 3. Translations + game params
```

