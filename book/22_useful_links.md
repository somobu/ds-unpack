# Other useful links

- [Compression algorithms detection](https://github.com/frizb/FirmwareReverseEngineering/blob/master/IdentifyingCompressionAlgorithms.md) to better understand how to handle DCX
- [ArrayMesh in Godot](https://docs.godotengine.org/en/stable/tutorials/3d/procedural_geometry/arraymesh.html#doc-arraymesh)
