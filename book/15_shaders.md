# Materials and shaders

![](res/material_to_shaders.png)

Notes:
- SPX files are not present in Release content;




## Mtd -> Spx -> Vpo/Fpo mapping



Complete list of spx:

```
FRPG_Ghost_Param
FRPG_Ghost_Tod
FRPG_NormalToAlpha
FRPG_Phn_ColDif
FRPG_Phn_ColDifBmp
FRPG_Phn_ColDifBmpLit
FRPG_Phn_ColDifBmpMul
FRPG_Phn_ColDifBmpMulLit
FRPG_Phn_ColDifLit
FRPG_Phn_ColDifMul
FRPG_Phn_ColDifMulLit
FRPG_Phn_ColDifSpc
FRPG_Phn_ColDifSpcBmp
FRPG_Phn_ColDifSpcBmpLit
FRPG_Phn_ColDifSpcBmpMul
FRPG_Phn_ColDifSpcBmpMulLit
FRPG_Phn_ColDifSpcLit
FRPG_Phn_ColDifSpcMul
FRPG_Phn_ColDifSpcMulLit
FRPG_Phn_FaceEye
FRPG_Snow
FRPG_Snow_Lit
FRPG_WaterWaveSfx
FRPG_Water_Env
FRPG_Water_Reflect
```

In case of `Phn` spx:

![](res/mtd_to_fpo.png)


## Spx name format

Name format:
```
FRPG_Phn_BASE
```


### BASE
- Sequence of flags. Flags not set are omitted;
- `Col` and `Dif` flags are always on;
- `Mul` flag requires `Spc` and `Bmp` flags to be set;

```
Col Dif Spc Bmp Mul Lit
```


## Vertex shader name format

Name format:
```
FRPG_Phn_BASE_A_B
```

Examples:
- FRPG_Phn_PIN_DL_Non.vpo
- FRPG_Phn_PINTT_DD_Vel.vpo
- FRPG_Phn_PIWNTT_DDL_DepAlp.vpo

### BASE
- Base is a sequence of flags;
- Each flag is written as a single letter and only if present (omitted otherwise);
- First two flags (`P` and `I`) are always on;
- `W` flag requires `N` to be set;

Flags are written in the following order:
```
PIWNTT
```

### A
Suffix A works in the same way as BASE:
```
DDL
```

### B
Suffix B might be one of 4 values, and for `Vel` and `Dep` value there's also `Alp` flag:

```
Non
Sdw
Vel Alp?
Dep Alp?
```

## Fragment shader name format

Name format:
```
FRPG_Phn_BASE_SUFFIX
```

Examples:
- FRPG_Phn_DifSpc___Mul___Sdw_Non.fpo
- FRPG_Phn_Dif___Bmp___Lit____HemEnvLerp.fpo
- FRPG_Phn_DifSpcBmpMulLitSdw_HemEnvLerpPntSSSS.fpo

### BASE

Base is a sequence of values. The first four values are essentialy flags, the last one has three states:
```
Dif             # Diffuse?, always enabled
Spc / ___       # Specular?
Bmp / ___       # Bump?
Mul / ___
Lit / ___       # Light?
Csd / Sdw / ___ # ... / Shadow?
```

### SUFFIX

- Suffix might be `Non` or `Hem`;
- None has no following values at all;
- After `Hem` must be either `Dir3` or `Env` or `EnvLerp`;
- And after that might be additional suffix: `PntS`, `PntSS` or `PntSSSS`

```
Non
Hem Dir3      PntS S SS
Hem Env       PntS S SS
Hem Env Lerp  PntS S SS
```