# Startup proccess

## Debug loading

Level loading proccess (presumably this is how game loads levels from debug menu)

1. Load all top-level archive indices (read .bhd5 files);
2. Read map list `/map/MapViewList.loadlistlist` from archive #0 (use index built from .bhd5
   to locate offset in .bdt file);
3. For entry chosen from loadlist read .msb specified by this entry from archive #0;
4. Instantiate parts from .msb;
5. ??? (yet to explore)

