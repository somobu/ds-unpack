# FLVER

> Model file

Contains only indices/vertices/bones, refers to materials and textures by path

![](../res/flver_structure.png)

Notes:
- Surface uses vertex buffer with the same index. It is possible that there are more surfaces than vertex buffers. 
  Supposedly, in this case vertex buffer index is calculated as `vb_i = surf_i % vb_count`.
- It looks like you can load entire Data section directly to GPU;
