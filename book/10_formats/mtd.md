# MTD

> Material definition

![](../res/mtd_structure.png)


Types:
- `block` is a fixed-size struct (20 bytes long);
- `mstr` stands for 'Marked string'. It is a Shift-JIS string (sequence of bytes) preceded by length of of this sequence and followed by some `marker` (marker value might may be different for different strings);
- `marker` is a `u8` that pads to `u32`. For example, we're expecting `0x35` marker between some `x` data and `y` data, so valid inputs are:
  - `xx xx xx xx | 35 00 00 00 | yy yy yy yy` - just a marker at the start of `u32` that pads `u32` with zeroes;
  - `xx xx xx xx | 35 00 FF FF | yy yy yy yy` - marker that pads with zeroes and `FF`s;
  - `xx xx xx xx | 35 AE 37 B9 | yy yy yy yy` - marker that pads with some random data (it is still valid!);
  - `xx xx xx xx | xx xx 35 00 | yy yy yy yy` - unaligned marker that pads `u32` with zeroes;
  - `xx xx xx xx | xx xx 35 77 | yy yy yy yy` - unaligned marker that pads with some random data (it is still valid!);

Notes:
- Warning: `length` field of `Block` might (and will) be larger than actual block size;
- In particular, this means that you can't walk over param or texture blocks array using `Block` `length`;
- So you really have to parse entire file. From start to end. No skipping possible;
- Yes, MTD is a crap of a format;



## .mtd.dcx

Notes:
- Is just a bnd3 archive holding a bunch of `.mtd`'s
- Record names are like `A17_Ground[DSB][L]_Alp.mtd`;

How to look up `MTD` referenced in `FLVER`:
  1. First, you get `MTD name` string from flver (say, you got `N:\FRPG\data\INTERROOT_win32\mtd\parts\P_DullLeather[DSB]_Edge.mtd`);
  2. Then, you stip all before last `\` (that makes a pretty `P_DullLeather[DSB]_Edge.mtd`);
  3. After that, you access `/mtd/Mtd.mtdbnd.dcx`, treating it as regular `bnd3` archive;
  4. You look up mtd name (`P_DullLeather[DSB]_Edge.mtd`) and find it;
