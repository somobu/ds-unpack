# Archives

## BHD+BDT
![](../res/archive_format.png)

Each game archive is stored in two files:

- small (<100kbytes) dvdbnd#**.bhd5** archive index file;
- large (up to 1.8 gbytes) dvdbnd#**.bdt** archive data file;

**.bhd5** index file contains records (grouped in buckets (1) ) -- each record is just a tuple
of `(name hash, size, offset)`. Each record references one of data blocks in **.bdt** file.

**.bdt** is just a small `BDF3` header followed by a consequent data blocks witten one by one. One can guess type of
data looking at first 4 bytes of block.

(1): records sorted in buckets using this formula: `bucket_index = name_hash % bucket_count`. Bucket count is specified
in BHD5 header at the top of the file; I suppose the bucket count is chosen in such a way as to leave no more than 15
records in a group -- to speed up lookup.

