# MSB1

> Map studio bundle

![](../res/msb1.png)

Details:
- There are four `Groups`, each `Group` contains `Records` of correspoding type:
    - Model: defines which model to use (`Parts` refer to these records);
    - Event: some data, unexplored yet;
    - Region: some area or trigger (unexplored too);
    - Part: instances of `Models`;
- `Group` is just a name and an array of pointers to `Records`;
- The last pointer in each `Group` points to the next `Group`;
- The last `Record` of the last `Group` (`Parts`) is the first `Group` (i.e. `Model Group`);
- From `Model` name you can derive FLVER path;

Notes:
- Format structure suggest single pass of proccessing;
- `PartEntry` stores rotation angles in DEGREES :cat_cry:;
