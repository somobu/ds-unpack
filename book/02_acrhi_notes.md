# Architecture notes

Ilya Somov, [9/19/24 8:02 PM]
Current setup is wrong

Ilya Somov, [9/19/24 8:02 PM]
Theres should be two modules, both hot-reloadable

Ilya Somov, [9/19/24 8:07 PM]
Renderer:
- the only mod that interacts w/ GPU;
- does all rendering- and gpu-related stuff;
- fn render: accepts scene tree, produces image on screen;
- fn load: accepts borrow on game archives, loads flvers and textures to GPU, copies flver headers;
- also theres should be hot-reload-related fcns, but this is not important for now;

Ilya Somov, [9/19/24 8:09 PM]
Game:
- mod that processes player input, updates scene tree, executes scripts and physics;
- passes scene tree to renderer;
- this mod is hot reloadable too;

Ilya Somov, [9/19/24 8:13 PM]
Occlusion/frustum culling is renderer's responsobility

Ilya Somov, [9/19/24 8:27 PM]
Correction: as the original game heavily relies on asset steaming, the second (loader) threas is required. This means that module interaction and overall architecture is a bit more complicated :/

Ilya Somov, [9/19/24 8:34 PM]
However, even single-threaded setup would be fine at the current stage
