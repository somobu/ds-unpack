#!/bin/bash
set -euxo pipefail

cargo doc --all-features --no-deps --workspace --document-private-items --open

