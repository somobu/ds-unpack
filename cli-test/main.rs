#![allow(dead_code)]

mod conversion_tool;

use std::{
    collections::{HashMap, HashSet},
    fs,
};

use conversion_tool::ParsedSpx;
use formats::{
    archive::RootArchive, bnd3::Bnd3, config::{self, GameType, UserConfig}, dcx, fhash, flver::Flver, loadlist, magic, millis, msb_2011::{model_part_type::ModelPartType, Msb2011}, msb_2015::Msb2015, mtd::Mtd, tpf::Tpf
};

fn main() {
    let cfg = config::get_user_config("user/cfg.json");
    if cfg.archive_names.is_some() {
        fhash::fill_revhash_map(&cfg.archive_names.clone().unwrap());
    }

    let s = millis();
    // models(&cfg);
    // mtd(&cfg);
    // flver(&cfg);
    bb(&cfg);

    let e = millis();
    println!("Elapsed {} ms", (e - s));
}

fn mtd(cfg: &UserConfig) {
    // let archive_0 = RootArchive::new(cfg, 0);
    let archive_1 = RootArchive::new(cfg, 1);
    // diff(&archive_1);
    // mtd2(&archive_1);
    // println!("{:#?}", shader_to_params);

    let parsed = ParsedSpx::Phn {
        col: true,
        dif: true,
        spc: true,
        bmp: true,
        mul: false,
        lit: false,
    };
    let shader = conversion_tool::get_vpo_name(&parsed);

    let shb_path = "/shader/FRPG_Flver_vpo.shaderbnd.dcx";
    let shb_archive = Bnd3::new(archive_1.data_by_name(&shb_path).unwrap());
    let shader_record = shb_archive
        .record_iter()
        .find(|e| e.name() == &shader)
        .unwrap();

    println!("Shader is {shader}");
    println!("Record is {:?}", shader_record.name());

    // for record in shb_archive.record_iter() {
    //     println!("{}", record.name());
    // }

    // let vpo_data = shb_archive.lookup("FRPG_Dbg_PINTT_DDL_Nrm.vpo").unwrap();
    // magic::dump_vec(vpo_data, "tmp/FRPG_Dbg_PINTT_DDL_Nrm.vpo").unwrap();
}

fn mtd2(archive_1: &RootArchive) {
    let mtd_path = "/mtd/Mtd.mtdbnd.dcx";
    let mtd_archive = Bnd3::new(archive_1.data_by_name(&mtd_path).unwrap());

    for record in mtd_archive.record_iter() {
        let mtd = Mtd::new(record.data());

        let parsed_spx = ParsedSpx::from_path(&mtd.shader_path);
        println!("{:24} -> {parsed_spx:?}", &mtd.shader_path[31..]);
    }
}

fn diff(archive_1: &RootArchive) {
    let mtd_path = "/mtd/Mtd.mtdbnd.dcx";
    let mtd_archive = Bnd3::new(archive_1.data_by_name(&mtd_path).unwrap());

    let mut shader_to_params: HashMap<String, HashSet<(String, String)>> = HashMap::new();
    let mut shader_params_diff: HashMap<String, HashSet<(String, String)>> = HashMap::new();

    for record in mtd_archive.record_iter() {
        let mtd = Mtd::new(record.data());

        let shader_path = mtd
            .shader_path
            .replace("N:\\FRPG\\data\\Material\\spx\\", "");

        // if shader_path.contains("Water") {
        //     continue;
        // }

        // if shader_path.contains("Snow") {
        //     continue;
        // }

        let mut name_type: HashSet<(String, String)> = mtd
            .params
            .iter()
            .map(|f| (f.name.clone(), f.r#type.clone()))
            .collect();

        for tex in mtd.textures {
            name_type.insert((
                tex.r#type,
                format!("tex/{}/{}", tex.uv_number, tex.shader_data_idx),
            ));
        }

        let p = shader_to_params.get(&shader_path);
        if p.is_some() {
            let p = p.unwrap();

            if *p != name_type {
                // println!("Mismatch on {}, refers to {}", record.name(), shader_path);

                let mut diff: HashSet<(String, String)> = p
                    .symmetric_difference(&name_type)
                    .into_iter()
                    .map(|f| f.clone())
                    .collect();

                // for (name, r#type) in &diff {
                //     println!("  {:6} {}", r#type, name);
                // }

                let old = shader_params_diff.get(&shader_path);
                if old.is_some() {
                    let old = old.unwrap();
                    for item in old {
                        diff.insert(item.clone());
                    }
                }

                shader_params_diff.insert(shader_path.clone(), diff);
            }
        } else {
            shader_to_params.insert(shader_path, name_type);
        }
    }

    let mut keys: Vec<&String> = shader_to_params.keys().into_iter().collect();
    keys.sort();

    for k in keys {
        println!("Shader {k}");

        // let mut v: Vec<&(String, String)> =
        //     shader_params_diff.get(k).unwrap().into_iter().collect();
        // v.sort();

        // for (name, r#type) in v {
        //     println!("  {:8} {}", r#type, name);
        // }
        // println!();
    }
}

fn models(cfg: &UserConfig) {
    let basename = "m14_00_00_00";

    let archive_0 = RootArchive::new(cfg, 0);
    let archive_1 = RootArchive::new(cfg, 1);

    let map_path = format!("/map/MapStudio/{basename}.msb");
    let msb = Msb2011::new(archive_0.data_by_name(&map_path).unwrap());

    let parts = msb.parts();

    for part_idx in 0..parts.entry_count() {
        let part = parts.entry_by_idx(part_idx);

        if part.entry_type == ModelPartType::Object {
            let name = part.name();
            let path = format!("/obj/{}.objbnd.dcx", &name[0..5]);
            println!("Looking for {}", path);

            let objbnd = archive_1.data_by_name(&path).unwrap();
            let binder = Bnd3::new(objbnd);
            println!("Binder flags {:b}", binder.flags());

            println!("\nRecords:");
            for record in binder.record_iter() {
                println!("  {} at 0x{:x}", record.name(), record.data_offset);
            }

            println!("\nTpf:");
            let tpf_data = binder.lookup(".tpf").unwrap().to_vec();
            let tpf = Tpf::new(tpf_data);
            for texture in tpf.texture_iter() {
                println!("  {}", texture.header.file_size);
            }

            println!("\nFlver:");
            let flver_data = binder.lookup(".flver").unwrap().to_vec();
            let flver = Flver::new(flver_data);
            flver.print_contents();

            break;
        }
    }
}

fn flver(cfg: &UserConfig) {
    let name = "/map/m14_00_00_00/m9999B0A14.flver.dcx";
    let archive_0 = RootArchive::new(cfg, 0);

    let data = archive_0.data_by_name(name).unwrap();
    let flver = Flver::new(data);

    // Header ver 0x2000c
    // Material count: 1
    //   Name sky
    //   MTD N:\FRPG\data\INTERROOT_win32\mtd\map\M[D].mtd
    //   GX 0
    //   2 textures
    //     g_Diffuse /map/m14/tex/m14_sky.tga
    //     g_DetailBumpmap
    // Bone count: 1
    //   bone
    // Mesh count: 1
    //   1 bones, 1 faces, 1 VBs
    // Face set count 1
    //   #0: 1594 indices
    // Vertex buffer count 1
    //   397 vertices, layout size 28
    // Buffer layout count 2
    //   layout from 0x1f0, len 5
    //   layout from 0x254, len 6

    // let mesh = flver.mesh(0);
    let vbuf = flver.vertex_buffer(0);

    let layout = flver.buffer_layout(vbuf.header.layout_index);
    for item in layout.members {
        println!(
            " #{}, {:?} {:?}, +{}",
            item.index, item.semantic, item.layout_type, item.struct_offset
        );
    }

    for vertex in vbuf.vertices() {
        let pair = unsafe { ((&vertex[24..]).as_ptr() as *const [u16; 2]).as_ref() }.unwrap();
        let floats = (pair[0] as f32 / 1024.0, pair[1] as f32 / 1024.0);
        println!("V {:X?} -> {:X?} -> {:?}", &vertex[24..], pair, floats);
    }

    println!();
}

fn bb(cfg: &UserConfig) {
    assert_eq!(cfg.game_type, GameType::BB);

    let loadlist = loadlist::from(resolve(cfg, "map/mapviewlist.loadlistlist"));
    let map = &loadlist[5];
    assert_eq!("map:/MapStudio/m22_00_00_00.msb", map.path);

    let msb_data = resolve(cfg, &map.path);
    magic::dump_vec(&msb_data, "user/msb.bin").unwrap();
    let msb = Msb2015::new(msb_data);
}

fn resolve(cfg: &UserConfig, subpath: &str) -> Vec<u8> {
    assert_eq!(cfg.game_type, GameType::BB);

    let norm_path;
    if subpath.starts_with("map:/") {
        norm_path = subpath.replace("map:/MapStudio/", "map/mapstudio/");
    } else {
        norm_path = subpath.to_string();
    }

    let mut file = format!("{}/{norm_path}", cfg.game_root);

    if fs::metadata(&file).is_ok() {
        println!("Resolved to {file}");
        std::fs::read(&file).unwrap()
    } else {
        file = file + ".dcx"; // Try w/ .dcx extension
        println!("Resolved to {file}");
        let data = std::fs::read(&file).unwrap();
        dcx::decompress(&data)
    }
}
