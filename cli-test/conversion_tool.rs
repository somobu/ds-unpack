#[derive(Debug)]
pub enum ParsedSpx {
    Ghost(SParamGhost),
    NormalToAlpha,
    Phn {
        col: bool,
        dif: bool,
        spc: bool,
        bmp: bool,
        mul: bool,
        lit: bool,
    },
    PhnFaceEye,
    Snow {
        lit: bool,
    },
    Water(SParamWater),
    WaterWaveSfx,
}

impl ParsedSpx {
    pub fn from_path(spx_path: &str) -> ParsedSpx {
        let mut spx = spx_path;

        let last_slash = spx.replace("\\", "/").rfind("/");
        if last_slash.is_some() {
            let last_slash = last_slash.unwrap();
            spx = &spx[last_slash + 1..];
        }

        assert!(spx.ends_with(".spx"));
        spx = &spx[..spx.len() - 4];

        assert!(spx.starts_with("FRPG_"));
        spx = &spx[5..];

        if spx.starts_with("Phn_") {
            spx = &spx[4..];

            if spx == "FaceEye" {
                ParsedSpx::PhnFaceEye
            } else {
                ParsedSpx::Phn {
                    col: spx.contains("Col"),
                    dif: spx.contains("Dif"),
                    spc: spx.contains("Spc"),
                    bmp: spx.contains("Bmp"),
                    mul: spx.contains("Mul"),
                    lit: spx.contains("Lit"),
                }
            }
        } else if spx == "Snow" {
            ParsedSpx::Snow { lit: false }
        } else if spx == "Snow_Lit" {
            ParsedSpx::Snow { lit: true }
        } else if spx == "Ghost_Param" {
            ParsedSpx::Ghost(SParamGhost::Param)
        } else if spx == "Ghost_Tod" {
            ParsedSpx::Ghost(SParamGhost::Tod)
        } else if spx == "Water_Env" {
            ParsedSpx::Water(SParamWater::Env)
        } else if spx == "Water_Reflect" {
            ParsedSpx::Water(SParamWater::Reflect)
        } else if spx == "NormalToAlpha" {
            ParsedSpx::NormalToAlpha
        } else if spx == "WaterWaveSfx" {
            ParsedSpx::WaterWaveSfx
        } else {
            unimplemented!("SPX: {spx}")
        }
    }
}

#[derive(Debug)]
pub enum SParamGhost {
    Param,
    Tod,
}

#[derive(Debug)]
pub enum SParamWater {
    Env,
    Reflect,
}

pub fn get_vpo_name(spx: &ParsedSpx) -> String {
    match spx {
        ParsedSpx::Ghost(v) => match v {
            SParamGhost::Param => "FRPG_Ghost_Skin.vpo".to_string(),
            SParamGhost::Tod => "FRPG_Ghost_Tod.vpo".to_string(),
        },
        ParsedSpx::NormalToAlpha => todo!(),
        ParsedSpx::Phn {
            col: _,
            dif: _,
            spc: _,
            bmp: _,
            mul: _,
            lit: _,
        } => "FRPG_Phn_PINT_D_Non.vpo".to_string(),
        ParsedSpx::PhnFaceEye => "FRPG_Phn_PINT_D_Non.vpo".to_string(),
        ParsedSpx::Snow { lit: _ } => todo!(),
        ParsedSpx::Water(_) => todo!(),
        ParsedSpx::WaterWaveSfx => todo!(),
    }
}

pub fn get_fpo_name(spx: &ParsedSpx) -> String {
    match spx {
        ParsedSpx::Ghost(_) => todo!(),
        ParsedSpx::NormalToAlpha => todo!(),
        ParsedSpx::Phn {
            col: _,
            dif: _,
            spc,
            bmp,
            mul,
            lit,
        } => {
            let spc = if *spc { "Spc" } else { "___" };
            let bmp = if *bmp { "Bmp" } else { "___" };
            let mul = if *mul { "Mul" } else { "___" };
            let lit = if *lit { "Lit" } else { "___" };

            let sdw = "Sdw";
            let suffix = "Non";

            format!("FRPG_Phn_Dif{spc}{bmp}{mul}{lit}{sdw}_{suffix}.fpo")
        }
        ParsedSpx::PhnFaceEye => todo!(),
        ParsedSpx::Snow { lit: _ } => todo!(),
        ParsedSpx::Water(_) => todo!(),
        ParsedSpx::WaterWaveSfx => todo!(),
    }
}
