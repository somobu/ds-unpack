mod app;
mod windowing;

#[cfg(not(feature = "hot_reload"))]
use game as game_hot;

#[cfg(feature = "hot_reload")]
#[hot_lib_reloader::hot_module(lib_dir = "target_hot/debug", dylib = "game")]
mod game_hot {

    pub use game::game::Game;
    pub use game::InputEvent;

    hot_functions_from_file!("game/src/lib.rs");

    #[lib_has_to_reload]
    pub fn has_to_reload() -> bool {}

    #[lib_reload]
    pub fn reload() {}
}

#[cfg(not(feature = "hot_reload"))]
use inspector as inspector_hot;

#[cfg(feature = "hot_reload")]
#[hot_lib_reloader::hot_module(lib_dir = "target_hot/debug", dylib = "inspector")]
mod inspector_hot {

    pub use game::game::Game;
    pub use inspector::InspectorOpaque;

    hot_functions_from_file!("inspector/src/lib.rs");

    #[lib_has_to_reload]
    pub fn has_to_reload() -> bool {}

    #[lib_reload]
    pub fn reload() {}
}

#[cfg(not(feature = "hot_reload"))]
use renderer as renderer_hot;

#[cfg(feature = "hot_reload")]
#[hot_lib_reloader::hot_module(lib_dir = "target_hot/debug", dylib = "renderer")]
mod renderer_hot {

    pub use game::game::Game;
    pub use renderer::RendererOpaque;
    pub use std::ffi::c_void;

    hot_functions_from_file!("renderer/src/lib.rs");

    #[lib_has_to_reload]
    pub fn has_to_reload() -> bool {}

    #[lib_reload]
    pub fn reload() {}
}

fn main() {
    let mut app = app::App::new();
    app.run();
}
