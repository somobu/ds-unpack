use sdl2::{
    video::{GLContext, SwapInterval, Window},
    EventPump, Sdl,
};
use std::ffi::c_void;

pub const PAD: u32 = 16;
pub const MAIN_W: u32 = 1280;
pub const MAIN_H: u32 = 720;
pub const INSP_OFS_X: u32 = MAIN_W + 2 * PAD;
pub const INSP_W: u32 = 1920 - 36 - INSP_OFS_X - PAD;
pub const INSP_H: u32 = 1080 - 4 * PAD;

#[allow(dead_code)]
pub struct WindowingState {
    pub sdl: Sdl,
    pub evt: EventPump,
    pub wnd_main: Window,
    pub inspector_wnd: Window,
    pub inspector_ctx: GLContext,
}

impl WindowingState {
    pub fn init() -> Self {
        let sdl = sdl2::init().unwrap();
        let vid = sdl.video().unwrap();
        let evt = sdl.event_pump().unwrap();

        // Make mouse click on unfocused window be registered as input event
        sdl2::hint::set("SDL_MOUSE_FOCUS_CLICKTHROUGH", "1");

        let mut wnd = vid
            .window("Dantelion [DXVK Renderer]", MAIN_W, MAIN_H)
            .position(PAD as i32, PAD as i32)
            .vulkan()
            .build()
            .unwrap();

        let inspector_wnd = vid
            .window("Dantelion [Inspector]", INSP_W, INSP_H)
            .position(INSP_OFS_X as i32, PAD as i32)
            .opengl()
            .borderless()
            .build()
            .unwrap();

        // Create a window context
        let ctx = inspector_wnd.gl_create_context().unwrap();
        inspector_wnd
            .subsystem()
            .gl_set_swap_interval(SwapInterval::Immediate)
            .unwrap();

        // Focus on main window
        wnd.raise();

        Self {
            sdl,
            evt,
            wnd_main: wnd,
            inspector_wnd,
            inspector_ctx: ctx,
        }
    }

    pub fn hwnd(&mut self) -> *mut c_void {
        self.wnd_main.raw() as *mut c_void
    }
}
