//! Global application context. Its like 'the actual Main file'

use crate::inspector_hot;
use crate::windowing::WindowingState;

use crate::game_hot::{game_input, game_update};
use crate::renderer_hot::{self, RendererOpaque};
use inspector::InspectorOpaque;
use sdl2::event::Event;

#[cfg(not(feature = "hot_reload"))]
use crate::game_hot::{game::Game, InputEvent};

#[cfg(feature = "hot_reload")]
use crate::game_hot::{Game, InputEvent};

pub struct App {
    wnd: WindowingState,
    game: Game,
    inspector: Option<InspectorOpaque>,
    renderer: Option<RendererOpaque>,
}

impl App {
    pub fn new() -> Self {
        let mut wnd = WindowingState::init();
        let game = Game::new();
        let inspector = inspector_hot::restore_state(&wnd.inspector_wnd, None);
        let renderer = renderer_hot::init(wnd.hwnd());

        Self {
            wnd,
            game,
            inspector: Some(inspector),
            renderer: Some(renderer),
        }
    }

    pub fn run(&mut self) {
        // In order to save some delicious FPS, inspector only invalidated after input events.
        // But to redraw things properly (e.g. w/ animations) we have to draw not a one, but a few frames.
        // 10 frames is good enough.
        let mut inspector_repaints_left = 10;

        loop {
            // Handle events
            loop {
                let e = self.wnd.evt.poll_event();
                if e.is_none() {
                    break;
                }

                match map_event(
                    e.unwrap(),
                    self.wnd.wnd_main.id(),
                    self.wnd.inspector_wnd.id(),
                ) {
                    MappedEvent::Exit => return,
                    MappedEvent::Ignore => {}

                    MappedEvent::PassToGame(event) => {
                        game_input(&mut self.game, event);
                    }

                    MappedEvent::PassToInspector(event) => {
                        inspector_hot::input(
                            self.inspector.as_mut().unwrap(),
                            &self.wnd.inspector_wnd,
                            event,
                        );
                        inspector_repaints_left = 10;
                    }
                }
            }

            // Reload modules
            #[cfg(feature = "hot_reload")]
            {
                if game_hot::has_to_reload() {
                    game_hot::reload();
                }

                if inspector_hot::has_to_reload() {
                    let mut old = self.inspector.take().unwrap();
                    let state = inspector_hot::save_state(&mut old);
                    drop(old);

                    inspector_hot::reload();

                    let new = inspector_hot::restore_state(&self.wnd.inspector_wnd, Some(state));
                    self.inspector = Some(new);

                    inspector_repaints_left = 10;
                }

                if renderer_hot::has_to_reload() {
                    let old_renderer = self.renderer.take();
                    drop(old_renderer);

                    renderer_hot::reload();

                    let new_renderer = renderer_hot::init(self.wnd.hwnd());
                    self.renderer = Some(new_renderer);
                }
            }

            // Update game state
            game_update(&mut self.game);

            // Render inspector
            if inspector_repaints_left > 0 {
                inspector_hot::render(
                    self.inspector.as_mut().unwrap(),
                    &self.wnd.inspector_wnd,
                    &mut self.game,
                );
                self.wnd.inspector_wnd.gl_swap_window();
                inspector_repaints_left -= 1;
            }

            // Render game
            if self.renderer.is_some() {
                renderer_hot::update(self.renderer.as_mut().unwrap(), &self.game);
            }
        }
    }
}

enum MappedEvent {
    Exit,
    Ignore,
    PassToGame(InputEvent),
    PassToInspector(sdl2::event::Event),
}

fn map_event(e: sdl2::event::Event, game_wnd_id: u32, inspector_wnd_id: u32) -> MappedEvent {
    match e {
        sdl2::event::Event::Quit { timestamp: _ } => MappedEvent::Exit,
        sdl2::event::Event::Window {
            timestamp: _,
            window_id,
            win_event,
        } => match win_event {
            sdl2::event::WindowEvent::Close => MappedEvent::Exit,
            _ => {
                if window_id == inspector_wnd_id {
                    MappedEvent::PassToInspector(e)
                } else {
                    MappedEvent::Ignore
                }
            }
        },
        _ => {
            if e.get_window_id().is_some_and(|v| v == game_wnd_id) {
                let input = sdl_to_game_input(e.clone());

                if input.is_some() {
                    MappedEvent::PassToGame(input.unwrap())
                } else {
                    MappedEvent::Ignore
                }
            } else {
                MappedEvent::PassToInspector(e)
            }
        }
    }
}

fn sdl_to_game_input(e: sdl2::event::Event) -> Option<InputEvent> {
    match e {
        Event::MouseMotion {
            timestamp: _,
            window_id: _,
            which: _,
            mousestate,
            x: _,
            y: _,
            xrel,
            yrel,
        } => Some(InputEvent::MouseMotion {
            dx: xrel as f32,
            dy: yrel as f32,
            is_rmb: mousestate.right(),
        }),
        Event::KeyDown {
            timestamp: _,
            window_id: _,
            keycode,
            scancode: _,
            keymod: _,
            repeat: _,
        } => Some(InputEvent::KeyDown {
            keycode: keycode?.into_i32(),
        }),
        Event::KeyUp {
            timestamp: _,
            window_id: _,
            keycode,
            scancode: _,
            keymod: _,
            repeat: _,
        } => Some(InputEvent::KeyUp {
            keycode: keycode?.into_i32(),
        }),

        _ => None,
    }
}
