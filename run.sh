#!/bin/bash
set -euxo pipefail

trap "trap - SIGTERM && kill -- -$$" SIGINT SIGTERM EXIT

export CARGO_TARGET_DIR="target_hot"
export RUSTFLAGS="-Awarnings"
export DXVK_LOG_LEVEL="warn"

# Quick 'cargo build' help:
# q: quiet mode
# r: release mode
# p: specify package

cargo watch -w game/src -x 'build -qp game' &
cargo watch -w inspector/src -x 'build -qp inspector' & 
cargo watch -w renderer/src -x 'build -qp renderer' & 
sleep 1 && cargo run -q --features "hot_reload"

kill $(jobs -p)
