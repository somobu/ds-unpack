use core::f32;

use egui::{Button, Checkbox, Context, Label, Layout, ScrollArea, Slider, TextEdit, Ui, Vec2};
use formats::msb_2011::model_part_type::ModelPartType;
use game::game::Game;

/// Note: `Default` is must-have here!
#[derive(Default, Clone, serde::Serialize, serde::Deserialize)]
pub struct Inspector {
    state: InspectorState,
    search_filter: String,
}

#[derive(Default, Clone, serde::Serialize, serde::Deserialize)]
enum InspectorState {
    #[default]
    MsbList,
    MsbDetails {
        lvl_name: String,
    },
}

impl Inspector {
    pub fn draw(&mut self, ctx: &Context, game: &mut Game) {
        // Reset hover filters
        game.filter_map = None;
        game.filter_entity = None;

        egui::CentralPanel::default().show(ctx, |ui| {
            ui.add(Checkbox::new(&mut game.use_vsync, "Vsync"));
            ui.add(Slider::new(&mut game.mist_dist, 50.0..=5000.0).logarithmic(true));
            ui.code(format!(
                "Pos: {:?}\nAng: {:?}",
                game.cam_position, game.cam_angle
            ));

            self.title(ui);

            let text = TextEdit::singleline(&mut self.search_filter)
                .margin(6.0)
                .hint_text("Search...")
                .desired_width(f32::INFINITY);
            ui.add(text);

            ui.separator();

            ScrollArea::vertical()
                .max_width(f32::INFINITY)
                .auto_shrink(false)
                .scroll_bar_visibility(egui::scroll_area::ScrollBarVisibility::AlwaysVisible)
                .show(ui, |ui| {
                    let state = self.state.clone();
                    match state {
                        InspectorState::MsbList => self.msb_list(ui, game),
                        InspectorState::MsbDetails { lvl_name } => {
                            self.msb_contents(ui, game, lvl_name)
                        }
                    };
                });
        });
    }

    fn title(&mut self, ui: &mut Ui) {
        ui.separator();

        let title = match &self.state {
            InspectorState::MsbList => "MSB List".to_string(),
            InspectorState::MsbDetails { lvl_name } => format!("Level: {}", lvl_name),
        };

        let back_target = match &self.state {
            InspectorState::MsbList => InspectorState::MsbList,
            InspectorState::MsbDetails { lvl_name: _ } => InspectorState::MsbList,
        };

        let button_width = 36.0;
        ui.with_layout(Layout::left_to_right(egui::Align::TOP), |ui| {
            let btn_size = Vec2::new(button_width, button_width);
            let button = Button::new("<");
            let cb = ui.add_sized(btn_size, button);
            if cb.clicked() {
                self.state = back_target;
            }

            let lbl_size = Vec2::new(ui.available_width(), button_width);
            ui.add_sized(lbl_size, Label::new(title));
        });
        ui.separator();
    }

    fn msb_list(&mut self, ui: &mut Ui, game: &mut Game) {
        // Tuple (map basename, japanese name)
        let known_lvls: Vec<(String, String)> = game
            .known_levels
            .iter()
            .map(|e| (e.map_basename(), e.name.clone()))
            .filter(|e| e.0.contains(&self.search_filter) || e.1.contains(&self.search_filter))
            .collect();

        for level in known_lvls {
            let is_loaded = game.loaded_levels.contains_key(&level.0);
            self.msb_list_item(ui, game, level.0, level.1, is_loaded);
            ui.separator();
        }
    }

    fn msb_list_item(
        &mut self,
        ui: &mut Ui,
        game: &mut Game,
        map_name: String,
        lvl_name: String,
        is_loaded: bool,
    ) {
        let button_width = 36.0;
        let padding = 10.0;
        let item_height = 36.0;

        let cb = ui.with_layout(
            Layout::left_to_right(egui::Align::TOP).with_cross_justify(false),
            |ui| {
                ui.with_layout(Layout::top_down(egui::Align::LEFT), |ui| {
                    ui.label(&map_name);
                    ui.label(&lvl_name);
                });

                // Spacer
                let label_size =
                    Vec2::new(ui.available_width() - 2.0 * (button_width + padding), 0.0);
                ui.add_sized(label_size, Label::new("").selectable(false));

                // Load/unload btn
                let btn_size = Vec2::new(button_width, item_height);
                let button = Button::new(if is_loaded { "X" } else { "[ ]" });
                let cb = ui.add_sized(btn_size, button);
                if cb.clicked() {
                    if is_loaded {
                        game.unload_level(&map_name);
                    } else {
                        game.load_level(&map_name);
                    }
                }

                // Level details btn
                if is_loaded {
                    let btn_size = Vec2::new(button_width, item_height);
                    let button = Button::new(">");
                    let cb = ui.add_sized(btn_size, button);
                    if cb.clicked() {
                        self.state = InspectorState::MsbDetails {
                            lvl_name: map_name.clone(),
                        };
                    }
                }
            },
        );

        if ui.rect_contains_pointer(cb.response.rect) && is_loaded {
            game.filter_map = Some(map_name.clone());
        }
    }

    fn msb_contents(&mut self, ui: &mut Ui, game: &mut Game, msb_name: String) {
        let msb = game.loaded_levels.get(&msb_name).unwrap();

        ui.label("Map parts");
        ui.separator();
        let parts = msb.parts();
        let d: Vec<(String, String)> = (0..parts.entry_count())
            .into_iter()
            .map(|i| {
                let part = parts.entry_by_idx(i);

                (
                    part.name().to_string(),
                    format!(
                        "{} {}",
                        model_part_type_human(&part.entry_type),
                        part.name(),
                    ),
                )
            })
            .filter(|e| e.0.contains(&self.search_filter) || e.1.contains(&self.search_filter))
            .collect();

        for e in d {
            self.msb_content_item(ui, game, e.0, e.1);
            ui.separator();
        }
    }

    fn msb_content_item(
        &mut self,
        ui: &mut Ui,
        game: &mut Game,
        entity_name: String,
        title_text: String,
    ) {
        let button_width = 36.0;
        let padding = 10.0;
        let item_height = 36.0;

        let cb = ui.with_layout(
            Layout::left_to_right(egui::Align::TOP).with_cross_justify(false),
            |ui| {
                ui.with_layout(Layout::top_down(egui::Align::LEFT), |ui| {
                    ui.label(&title_text);
                });

                // Spacer
                let label_size =
                    Vec2::new(ui.available_width() - 2.0 * (button_width + padding), 0.0);
                ui.add_sized(label_size, Label::new("").selectable(false));

                // Jump to entry btn
                let btn_size = Vec2::new(button_width, item_height);
                let button = Button::new("!");
                let cb = ui.add_sized(btn_size, button);
                if cb.clicked() {
                    println!("TODO: jump to entry");
                }

                // Entry details btn
                let btn_size = Vec2::new(button_width, item_height);
                let button = Button::new(">");
                let cb = ui.add_sized(btn_size, button);
                if cb.clicked() {
                    println!("TODO: inspect entry");
                }
            },
        );

        if ui.rect_contains_pointer(cb.response.rect.expand2(Vec2 { x: -16.0, y: 8.0 })) {
            game.filter_entity = Some(entity_name.clone());
        }
    }
}

fn model_part_type_human(mpt: &ModelPartType) -> String {
    format!("{:?}", mpt)
}
