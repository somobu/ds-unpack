use std::time::Instant;

use egui::{Context, FullOutput};
use egui_sdl2_gl::{gl, painter::Painter, with_sdl2, DpiScaling, EguiStateHandler, ShaderVersion};
use game::game::Game;

use crate::inspector::Inspector;

pub struct Wrapper {
    painter: Painter,
    state: EguiStateHandler,
    ctx: Context,
    start_time: Instant,

    pub data: Inspector,
}


impl Wrapper {
    pub fn new(wnd: &sdl2::video::Window) -> Wrapper {
        let (painter, state) = with_sdl2(wnd, ShaderVersion::Default, DpiScaling::Default);

        let ctx = egui::Context::default();
        // ctx.set_debug_on_hover(true);

        Wrapper {
            painter,
            state,
            ctx,
            start_time: Instant::now(),
            data: Default::default(),
        }
    }

    pub fn input(&mut self, wnd: &sdl2::video::Window, event: sdl2::event::Event) {
        self.state.process_input(wnd, event, &mut self.painter)
    }

    pub fn render(&mut self, wnd: &sdl2::video::Window, game: &mut Game) {
        unsafe {
            gl::ClearColor(0.0, 0.0, 0.0, 1.0);
            gl::Clear(gl::COLOR_BUFFER_BIT);
        }

        self.state.input.time = Some(self.start_time.elapsed().as_secs_f64());
        self.ctx.begin_frame(self.state.input.take());

        self.data.draw(&self.ctx, game);

        let FullOutput {
            platform_output,
            textures_delta,
            shapes,
            pixels_per_point,
            ..
        } = self.ctx.end_frame();

        // Process ouput
        self.state.process_output(wnd, &platform_output);

        let paint_jobs = self.ctx.tessellate(shapes, pixels_per_point);
        self.painter.paint_jobs(None, textures_delta, paint_jobs);
    }
}
