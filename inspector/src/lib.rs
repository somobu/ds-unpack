use game::game::Game;
use inspector::Inspector;
use wrapper::Wrapper;

mod inspector;
mod wrapper;

pub struct InspectorOpaque {
    ptr: usize,
}

impl InspectorOpaque {
    fn init(wnd: &sdl2::video::Window) -> Self {
        let inspector = Wrapper::new(wnd);
        let b = Box::new(inspector);
        let raw = Box::into_raw(b);
        let ptr = raw as usize;
        InspectorOpaque { ptr }
    }

    fn access_mut(&mut self) -> &mut Wrapper {
        unsafe { (self.ptr as *mut Wrapper).as_mut().unwrap() }
    }
}

impl Drop for InspectorOpaque {
    fn drop(&mut self) {
        let other_data = Inspector::default();
        let _ = std::mem::replace(&mut self.access_mut().data, other_data);

        // This memory leak is intentional
        // Normally, here we have to:
        //
        // let b = unsafe { Box::from_raw(self.ptr as *mut Wrapper) };
        // drop(b);
        //
        // But this will cause egui-sdl to crash :/
    }
}

#[no_mangle]
pub fn restore_state(wnd: &sdl2::video::Window, data: Option<String>) -> InspectorOpaque {
    let mut opaq = InspectorOpaque::init(wnd);

    if data.is_some() {
        let data = data.unwrap();
        let data: Inspector = serde_json::from_str(&data).unwrap_or_default();
        opaq.access_mut().data = data;
    }

    opaq
}

#[no_mangle]
pub fn save_state(state: &mut InspectorOpaque) -> String {
    let json = serde_json::to_string(&state.access_mut().data).unwrap();
    json
}

#[no_mangle]
pub fn input(state: &mut InspectorOpaque, wnd: &sdl2::video::Window, event: sdl2::event::Event) {
    state.access_mut().input(wnd, event);
}

#[no_mangle]
pub fn render(state: &mut InspectorOpaque, wnd: &sdl2::video::Window, game: &mut Game) {
    state.access_mut().render(wnd, game);
}
