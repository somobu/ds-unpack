use game::Game;

pub mod game;

pub enum InputEvent {
    MouseMotion { dx: f32, dy: f32, is_rmb: bool },
    KeyUp { keycode: i32 },
    KeyDown { keycode: i32 },
}

#[no_mangle]
pub fn game_input(game: &mut Game, event: InputEvent) {
    game.input(event);
}

#[no_mangle]
pub fn game_update(game: &mut Game) {
    game.update();
}

#[no_mangle]
pub fn game_load_level(game: &mut Game, map_name: &str) {
    game.load_level(map_name);
}
