use std::collections::HashMap;

use formats::{archive::RootArchive, config, loadlist, msb_2011::Msb2011};
use nalgebra::{Point3, Rotation3, Vector3};

use crate::InputEvent;

pub struct Game {
    archives: [RootArchive<'static>; 4],

    pub use_vsync: bool,

    pub cam_position: Point3<f32>,

    /// In radians
    pub cam_angle: [f32; 2],
    pub cam_move: [f32; 3],

    pub mist_dist: f32,

    pub filter_map: Option<String>,
    pub filter_entity: Option<String>,

    pub known_levels: Vec<loadlist::Entry>,

    /// Maps level name -> level description
    pub loaded_levels: HashMap<String, Msb2011>,
}

impl Game {
    pub fn new() -> Self {
        let archives = load_archives();

        let mut known_levels = loadlist::from(
            archives[0]
                .data_by_name("/map/MapViewList.loadlistlist")
                .unwrap(),
        );
        known_levels.sort_by(|a, b| {
            let a = a.map_basename();
            let b = b.map_basename();
            a.partial_cmp(&b).unwrap()
        });

        Self {
            archives,
            use_vsync: true,
            cam_position: Point3::new(-329.93, -122.22, 138.57),
            cam_angle: [-0.035, -1.36],
            cam_move: [0.0, 0.0, 0.0],
            mist_dist: 500.0,
            known_levels,
            loaded_levels: HashMap::new(),
            filter_map: None,
            filter_entity: None,
        }
    }

    pub(crate) fn input(&mut self, event: InputEvent) {
        match event {
            InputEvent::MouseMotion { dx, dy, is_rmb } => {
                if is_rmb {
                    self.cam_angle[0] -= 0.005 * dy;
                    self.cam_angle[1] += 0.005 * dx;
                }
            }
            InputEvent::KeyDown { keycode } => {
                match keycode {
                    119 => self.cam_move[2] = -1.0, // W
                    115 => self.cam_move[2] = 1.0,  // S
                    97 => self.cam_move[0] = 1.0,   // A
                    100 => self.cam_move[0] = -1.0, // D
                    113 => self.cam_move[1] = -1.0, // Q
                    101 => self.cam_move[1] = 1.0,  // E

                    _ => {}
                }
            }
            InputEvent::KeyUp { keycode } => {
                match keycode {
                    115 | 119 => self.cam_move[2] = 0.0, // WS
                    97 | 100 => self.cam_move[0] = 0.0,  // AD
                    113 | 101 => self.cam_move[1] = 0.0, // QE

                    _ => {}
                }
            }
        }
    }

    pub(crate) fn update(&mut self) {
        let move_delta = Rotation3::from_euler_angles(self.cam_angle[0], self.cam_angle[1], 0.0)
            * Vector3::new(self.cam_move[0], self.cam_move[1], self.cam_move[2])
            * 60.0;
        self.cam_position += move_delta * (1.0f32 / 60.0);
    }

    /// Ex: m14_00_00_00
    pub fn load_level(&mut self, map_name: &str) {
        let msb_name = format!("/map/MapStudio/{map_name}.msb");
        println!("Game: loading level {msb_name}");

        let msb = Msb2011::new(self.archives[0].data_by_name(&msb_name).unwrap());
        self.loaded_levels.insert(map_name.to_string(), msb);
    }

    pub fn unload_level(&mut self, map_name: &str) {
        self.loaded_levels.remove(map_name);
    }
}

fn load_archives() -> [RootArchive<'static>; 4] {
    let cfg = config::get_user_config("./user/cfg.json");

    [
        RootArchive::new(&cfg, 0),
        RootArchive::new(&cfg, 1),
        RootArchive::new(&cfg, 2),
        RootArchive::new(&cfg, 3),
    ]
}
